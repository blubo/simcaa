import React, { Component,Fragment } from 'react'
import { Modal,Button, Icon, Popup, Form, Input } from 'semantic-ui-react'

import PositiveMessage from '../components/PositiveMessage'
import NegativeMessage from '../components/ErrorMessage'

import {apolloFetch} from '../../store/apolloFetchUtils'

import {connect} from 'react-redux'
import {saveTeams} from '../../store/actions/AdministrationActions'

class AddUserModal extends Component {
  constructor(props){
    super(props)
    this.state={
      done: false,
      error: false,
      loading: false,
      dataWritten: false,
      teamName: '',
      teamMail: ''
    }
  }
  createTeam(){
    let query = `
      mutation createTeam {
          createCaaTeam(
                type: 0,
                idstaff: ${this.props.user.idstaff},
                name: "${this.state.teamName}",
                email: "${this.state.teamMail}"
              ){
            id
          }
      }
    `
    this.setState({loading: true})
    apolloFetch({query})
      .then((data)=>{
        if(data.hasOwnProperty("errors")){
          // errors in creating the team
          this.setState({done:false, error:true, loading: false})
        }else{
         query = `
            mutation updateUser {
                createCaaUserTeam(iduser: ${this.props.user.id}, idteam: ${data.data.createCaaTeam.id}) {
                  id
                }
            }
          `
          this.setState({loading: true})
          apolloFetch({query})
            .then(data => {
              if(data.hasOwnProperty("errors")){
                // errors in creating the team
                this.setState({done:false, error:true, loading: false})
              } else {
                // no errors, all ok
                this.setState({done:true, error:false},()=>{this.props.saveTeamsToStore()})
              }
            })
        }
      })
      .catch((error)=>{
        this.setState({done:false, error:true, loading:false})
      })
  }
  nameChange(e){
      this.setState({
          teamName: e.target.value,
          dataWritten: true 
      })
  }
  mailChange(e){
      this.setState({
          teamMail: e.target.value,
          dataWritten: true 
      })
  }
  getModalContent(){
    if(!this.state.done && !this.state.error){
      //user yet to Search
      return(
        <Fragment>
          <p>Inserisci i dati del nuovo team:</p>
          <Form>
            <Form.Field control={Input} label='Nome del Team' value={this.state.teamName} onChange={this.nameChange.bind(this)}/> 
            <Form.Field control={Input} label='Email' value={this.state.teamMail} onChange={this.mailChange.bind(this)}/>      
          </Form>
        </Fragment>
      )
    }else if(this.state.done){
      //user added to team
      return(
        <PositiveMessage header="Tutto ok!" message="Il team è stato creato correttamente :)" hidden={false}/>
      )
    }else if(this.state.error) {
      //errors in the operation
      return(
        <NegativeMessage header="Errore!" message="Impossibile creare il team, riprova più tardi :(" hidden={false}/>
      )
    }
  }

  render(){
    let contentLayout = this.getModalContent()
    return(
      <Modal closeIcon="close" open={this.props.openModal} onClose={this.props.closeModal}>
        <Modal.Header>
          Aggiunta di un utente per il team {this.state.teamName}
          <Popup trigger={<Icon name='help' style={{float: 'right'}}/>}
            content={"In questa tabella sono mostrati gli utenti del sistema che non appartengono al team "+this.state.teamName+", cliccando sulla riga di un utente questi verrà selezionato, per aggiungerlo in questo team basterà poi cliccare su Aggiungi."}/>
        </Modal.Header>
        <Modal.Content>
          {contentLayout}
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={()=>this.props.closeModal()}>Chiudi</Button>
          {
            !this.state.done && !this.state.error ?
            <Button positive loading={this.state.loading} disabled={!this.state.dataWritten} onClick={()=>this.createTeam()}>Crea il Team</Button>
            : null
          }
        </Modal.Actions>
      </Modal>
    )
  }
}
const mapStateToProps = (state)=>{
  return{
    user: state.user
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    saveTeamsToStore: ()=> dispatch(saveTeams())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddUserModal)
