import React, { Component } from 'react'
import { Modal, Button, Icon, Popup } from 'semantic-ui-react'

import PositiveMessage from '../components/PositiveMessage'
import NegativeMessage from '../components/ErrorMessage'
import ManageTeamTable from './ManageTeamTable'

import {apolloFetch} from '../../store/apolloFetchUtils'
import {connect} from 'react-redux'
import {saveTeams, saveRoles} from '../../store/actions/AdministrationActions'
import { translate } from 'react-i18next'

class ManageTeamModal extends Component {
  constructor(props){
    super(props)
    this.state={
      error: false,
      errorUserInOneTeam: false,
      loading: false,
      teamName: ''
    }
  }


  componentDidMount(){
    this.props.teams.forEach((team)=>{
      if(team.id === this.props.toTeam){
        this.setState({teamName: team.name})
      }
    })
  }
  removeUser(userteam_id){
    //removes user from team
    if(userteam_id === -1){
      this.setState({errorUserInOneTeam: true})
    }else{
      let query = `
      mutation removeFromTeam{
        deleteCaaUserTeam(id: ${userteam_id}){
          id
        }
      }
      `
      this.setState({loading:true})
      apolloFetch({ query })
      .then((data) => {
        if(data.hasOwnProperty("errors")){
          this.setState({removed: false, loading: false, error: true});
        }else{
          this.setState({removed: true, loading: false, error: false});
        }
      })
      .catch((error) => {
        this.setState({removed: false, loading: false, error: true});
      })
    }
  }
  closeMessage(which){
    switch(which){
      case 1:
        this.setState({removed: false})
        break
      case 2:
        this.setState({error: false})
        break
      case 3:
        this.setState({errorUserInOneTeam: false})
        break
    }
  }
  render(){
    return(
      <Modal closeIcon="close" open={this.props.openModal} onClose={this.props.closeModal}>
        <Modal.Header>
          Gestione del team <b>{this.state.teamName}</b>
          <Popup trigger={<Icon name='help' style={{float: 'right'}}/>}
            content={"In questa tabella sono visualizzati gli utenti appartenenti al team "+this.state.teamName+", è possibile tramite gli appositi tasti eliminare un utente dal team oppure modificare i parametri e i permessi di un utente (attenzione, i parametri/permessi dell'utente non sono modificati solamente per questo team!)"}/>

        </Modal.Header>
        <Modal.Content>
          <ManageTeamTable
            removeUser={this.removeUser.bind(this)}
          />
        <PositiveMessage header="Tutto ok!" message="Utente eliminato correttamente dal team :)" hidden={!this.state.removed} onDismiss={()=>this.closeMessage(1)}/>
          <NegativeMessage header="Errore!" message="Non è stato possibile rimuovere l'utente da questo team, riprova più tardi" hidden={!this.state.error} onDismiss={()=>this.closeMessage(2)}/>
          <NegativeMessage header="Errore!" message="Non è possibile rimuovere questo utente, appartiene solamente a questo team" hidden={!this.state.errorUserInOneTeam} onDismiss={()=>this.closeMessage(3)}/>
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={()=>this.props.closeModal()} loading={this.state.loading}>Chiudi</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
const mapStateToProps = (state)=>{
  return{
    curTeam: state.modalReducer.toTeam,
    teams: state.adminReducer.teams
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    saveTeamsToStore: ()=> dispatch(saveTeams()),
    saveRolesToStore: ()=> dispatch(saveRoles()),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(translate('translations')(ManageTeamModal))
