import React, { Component,Fragment } from 'react'
import { Modal,Button,Message, Icon, Popup } from 'semantic-ui-react'

import PositiveMessage from '../components/PositiveMessage'
import NegativeMessage from '../components/ErrorMessage'

import UsersTable from '../components/UsersTable'
import {apolloFetch} from '../../store/apolloFetchUtils'

import {connect} from 'react-redux'
import {saveUserToAdd} from '../../store/actions/AdministrationActions'

class AddUserModal extends Component {
  constructor(props){
    super(props)
    this.state={
      done: false,
      error: false,
      loading: false,
      userToAdd: -1,
      users: []
    }
  }
  addUser(){
    let query = `
      mutation updateUser {
          createCaaUserTeam(iduser: ${this.state.userToAdd}, idteam: ${this.props.curTeam}) {
            id
          }
      }
    `
    this.setState({loading: true})
    apolloFetch({query})
      .then((data)=>{
        this.setState({loading: false})
        if(data.hasOwnProperty("errors")){
          //errors in creating the association user->team
          this.setState({done:false, error:true})
        }else{
          //no errors, all ok
          this.setState({done:true, error:false})
        }
      })
      .catch((error)=>{
        this.setState({done:false, error:true, loading:false})
      })
  }
  componentDidMount(){
    this.props.saveUserToAddToStore(this.props.user.idstaff, this.props.curTeam)
    this.setState({users: this.props.user_to_add})
  }
  userSelected(id){
    this.setState({userToAdd: id},()=>{this.forceUpdate()})
  }
  getModalContent(){
    if(!this.state.done && !this.state.error){
      //user yet to Search
      return(
        <Fragment>
          <p>Seleziona l'utente da inserire nel team, ti basta cliccare sulla sua riga</p>
          <UsersTable
            users={this.props.user_to_add}
            userSelected = {this.userSelected.bind(this)}
            selectable={true}
            manageUsers={false}
            modUser={()=>{return null}}
            removeUser={()=>{return null}}/>
          <Message positive hidden={this.state.userToAdd === -1 ? true : false}>
            <p>
              Hai selezionato l'utente con id: {this.state.userToAdd}
            </p>
          </Message>
        </Fragment>
      )
    }else if(this.state.done){
      //user added to team
      return(
        <PositiveMessage header="Tutto ok!" message="L'utente è stato aggiunto correttamente al team :)" hidden={false}/>
      )
    }else if(this.state.error) {
      //errors in the operation
      return(
        <NegativeMessage header="Errore!" message="L'utente non è stato aggiunto al team, riprova più tardi :(" hidden={false}/>
      )
    }
  }

  render(){
    let contentLayout = this.getModalContent()
    return(
      <Modal closeIcon="close" open={this.props.openModal} onClose={this.props.closeModal}>
        <Modal.Header>
          Aggiunta di un utente per il team {this.state.teamName}
          <Popup trigger={<Icon name='help' style={{float: 'right'}}/>}
            content={"In questa tabella sono mostrati gli utenti del sistema che non appartengono al team "+this.state.teamName+", cliccando sulla riga di un utente questi verrà selezionato, per aggiungerlo in questo team basterà poi cliccare su Aggiungi."}/>
        </Modal.Header>
        <Modal.Content>
          {contentLayout}
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={()=>this.props.closeModal()}>Chiudi</Button>
          {
            !this.state.done && !this.state.error ?
            <Button positive loading={this.state.loading} disabled={this.state.userToAdd === -1 ? true : false } onClick={()=>this.addUser()}>Aggiungi Utente</Button>
            : null
          }
        </Modal.Actions>
      </Modal>
    )
  }
}
const mapStateToProps = (state)=>{
  return{
    user_to_add: state.adminReducer.user_to_add,
    curTeam: state.modalReducer.toTeam,
    user: state.user
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    saveUserToAddToStore: (staff_id, notInTeam)=> dispatch(saveUserToAdd(staff_id, notInTeam))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddUserModal)
