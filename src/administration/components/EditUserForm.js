import React, { Component } from 'react'
import {Form, Accordion, Input, Select, Icon, Label, Checkbox} from 'semantic-ui-react'

class EditUserForm extends Component{
  constructor(props){
    super(props)
    this.state={
      roles: [],
      modPass: -1,
      oldPassword: '',
      newPassword: '',
      repeatPassword: ''
    }
  }

  componentDidMount(){
    let roleIndex = this.props.roles.findIndex(role => role.id === this.props.loggedRole_id)
    let tmpRoles = this.props.roles.filter(role => this.props.roles[roleIndex].weight >= role.weight)
    tmpRoles = tmpRoles.map((role)=>{
      return ({text: role.role_desc, value: role.id})
    })

    this.setState({roles: tmpRoles})
  }

  handlePassword(type, e) {
    switch (type) {
      case 'old':
          this.setState({oldPassword: e.target.value})
          this.props.oldPassChange(e.target.value)
        break
        case 'new':
          this.setState({newPassword: e.target.value})
          this.props.newPassChange(e.target.value)
        break
      case 'repeat':
          this.setState({repeatPassword: e.target.value})
          this.props.repeatPassChange(e.target.value)
        break
      default:
        break
    }
  }

  render(){
    let labelPassword
    if (this.state.oldPassword !== '' || this.state.newPassword !== '' || this.state.repeatPassword !== '') {
      if (this.state.newPassword === this.state.repeatPassword) {
        labelPassword = <Label color='green' pointing='left'>Le password inserite coincidono!</Label>          
      } else {
        labelPassword = <Label color='red' pointing='left'>Le password inserite non coincidono!</Label>
      }
    }
    return(
        <Form>
            <Form.Group widths='equal'>
              <Form.Field control={Input} label='Nome' value={this.props.name} onChange={this.props.nameChange()}/>
              <Form.Field control={Input} label='Email' value={this.props.email} onChange={this.props.emailChange()}/>
              {
                this.props.mode && this.props.mode === 'admin' ?
                <Form.Field control={Input} label='Username' value={this.props.username} onChange={this.props.usernameChange()}/>
                : null
              }
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Field control={Input} label='Organizzazione' value={this.props.organization} onChange={this.props.orgChange()}/>
              <Form.Field control={Input} label='Sito Web' value={this.props.link_web} onChange={this.props.linkWebChange()}/>
              {
                this.props.mode && this.props.mode === 'admin' ?
                <Form.Field control={Select} label='Ruolo' options={this.state.roles} onChange={this.props.roleChange()} defaultValue={this.props.role_id}/>
                : null
              }
            </Form.Group>
            {
              !this.props.self ?
              <Form.Field control={Checkbox} label='Utente bloccato' toggle
                checked={this.props.status === 1 ? true : false}
                onChange={this.props.statusChange.bind(this)}
              />
              : null
            }
            <Accordion as={Form.Field}>
              <Accordion.Title active={this.state.modPass === 0} index={0} onClick={(e, titleProps)=>{titleProps.index === this.state.modPass ? this.setState({modPass: -1}) : this.setState({modPass: 0})}}>
                <Icon name='dropdown'/>
                Modifica password
              </Accordion.Title>
              <Accordion.Content active={this.state.modPass === 0}>
                {
                  this.props.mode && this.props.mode === 'user' ?
                  <Form.Field inline width={16}>
                    <input placeholder='Vecchia password' onChange={this.props.oldPassChange()} type='password' style={{width: '50%'}}/>
                  </Form.Field>
                  : null
                }
                <Form.Field width={16}>
                  <label>Nuova password</label>
                  <input placeholder='Nuova password' value={this.state.newPassword}
                    onChange={this.handlePassword.bind(this, 'new')}
                    type='password' style={{width: '50%'}}
                  />
                  {labelPassword}
                </Form.Field>
                <Form.Field width={16}>
                  <label>Ripeti password</label>
                  <input placeholder='Ripeti password' value={this.state.repeatPassword}
                    onChange={this.handlePassword.bind(this, 'repeat')}
                    type='password' style={{width: '50%'}}
                  />
                  {labelPassword}
                </Form.Field>
              </Accordion.Content>
            </Accordion>
        </Form>
    )
  }
}

export default EditUserForm