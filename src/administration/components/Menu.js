import React, { Component } from 'react'
import { translate } from 'react-i18next'
import { Menu } from 'semantic-ui-react'
import { withRouter, Link } from 'react-router-dom'

import LanguageSwitcher from '../../LanguageSwitcher'
import StaffSwitcher from '../../containers/switcher/StaffSwitcher'
import Can from '../../containers/Permission'

class AdminMenu extends Component{
  Logout() {
    sessionStorage.removeItem('state')
    sessionStorage.removeItem('jwt')
    this.props.history.push('/')
  }
  render(){
    const { t } = this.props

    return(
        <Menu id='navbar'>
            <Menu.Item name='header'>
                <h3><b>Pannello di Amministrazione</b></h3>
            </Menu.Item>
            <Menu.Item as={Link} to="/admin/home">
                Home
            </Menu.Item>
            <Menu.Item as={Link} to="/admin/overview">
                Amministra
            </Menu.Item>
            <Menu.Menu position='right'>
              <Menu.Item name='goback' onClick={() => this.props.returnToProject()}>
                {t("ADM_MNU_BACK")}
              </Menu.Item>

              <Can perform='can_manage_staff'>
                <StaffSwitcher sort='name' />
              </Can>
              <LanguageSwitcher type='dropdown' />

              <Menu.Item name='logout' onClick={() => this.Logout()}>
                  <b>Logout</b>
              </Menu.Item>
            </Menu.Menu>

        </Menu>
    )
  }
}
export default translate('translations') (withRouter(AdminMenu))
