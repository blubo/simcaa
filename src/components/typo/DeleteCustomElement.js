import React, { Component } from 'react'
import { Modal, Button, Table, Icon, Image } from 'semantic-ui-react'
import { translate } from 'react-i18next'

class DeleteCustomElement extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
        }
    }

    // Handle open and close of the modal
    openCloseModal() {
        this.setState({open: !this.state.open})
    }

    // Call the props function to delete the selected text
    deleteCurrentText(text, e) {
        this.props.deleteText(text)
        this.openCloseModal()
    }

    // Call the props function to delete the selected image
    deleteCurrentImage(image, e) {
        this.props.deleteImage(image)
        this.openCloseModal()
    }

    render() {
        const { t } = this.props

        let dataTableText = this.props.text
        dataTableText = dataTableText.map((item, index) => {
            let text = item.text.length > 50 ? item.text.substring(0,50) + '...' : item.text
            return (
                <Table.Row key={index}>
                    <Table.Cell collapsing>Text</Table.Cell>
                    <Table.Cell>
                        <div dangerouslySetInnerHTML={{__html: text}}></div>
                    </Table.Cell>
                    <Table.Cell collapsing>
                        <Icon name='trash' color='red' size='big' className='icon-pointer'
                            onClick={this.deleteCurrentText.bind(this, item)}
                        />
                    </Table.Cell>
                </Table.Row>
            )
        })

        let dataTableImage = this.props.image
        dataTableImage = dataTableImage.map((item, index) => {
            let name
            if (item.old) {
                name = item.name
            } else {
                name = this.props.projectid + '_' + this.props.chapterid + '_' + item.name
            }
            let src = window.env.MediaImage + this.props.projectid + '/' + name
            return (
                <Table.Row key={index}>
                    <Table.Cell collapsing>Image</Table.Cell>
                    <Table.Cell><Image src={src} size='tiny'/></Table.Cell>
                    <Table.Cell collapsing>
                        <Icon name='trash' color='red' size='big' className='icon-pointer'
                            onClick={this.deleteCurrentImage.bind(this, item)}
                        />
                    </Table.Cell>
                </Table.Row>
            )
        })

        return(
            <Modal trigger={<div color='yellow'
                            onClick={this.openCloseModal.bind(this)}
                            disabled={this.props.disabled}
                            style={this.props.style}
                            className={this.props.className}
                            >
                                {t("TYPO_HDR_DELETECUSTOM")}
                            </div>}
                open={this.state.open}
            >
                <Modal.Header>{t("TYPO_HDR_DELETECUSTOM")}</Modal.Header>
                <Modal.Content>
                    <Table celled striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Type</Table.HeaderCell>
                                <Table.HeaderCell>Title</Table.HeaderCell>
                                <Table.HeaderCell>Actions</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {dataTableText}
                            {dataTableImage}
                        </Table.Body>
                    </Table>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative onClick={this.openCloseModal.bind(this)}>{t("HEAD_BTN_CLOSE")}</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default translate('translations')(DeleteCustomElement)
