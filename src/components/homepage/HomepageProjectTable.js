import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Pagination, Input, Icon, Button, List, Radio, Loader } from 'semantic-ui-react'
import { translate } from 'react-i18next'
import { Link } from 'react-router-dom'

import NewProjectForm from '../../containers/homepage/NewProjectForm'
import Can from '../../containers/Permission'

class HomepageProjectTable extends Component {
    constructor(props) {
        super(props)
        this.state= {
            projects: props.project,
            valueSearch: '',
            displayFilter: false,

            // Pagination state variables
            totalPages: 5,
            total: 0,
            limit: 15,
            page: props.page,

            // Sort Table variables
            direction: 'ascending',
            column: null,
        }
    }

    componentDidMount() {
        this.setState({totalPages: Math.ceil(this.props.total / this.state.limit), total: this.props.total})
    }

    componentDidUpdate(prevProps) {
        if (this.props.project !== prevProps.project) {
            this.setState({ projects: this.props.project })
        }
        if (this.props.total !== prevProps.total) {
            this.componentDidMount()
        }
    }

    // Sort the Project Table
    handleSort(column, e) {
        if (this.state.column !== column) {
            let localProject = Object.assign(this.state.projects).sort((a, b) => {
                return a[column] - b[column]
            })
            this.setState({column, direction: 'ascending', projects: localProject})
        } else {
            this.setState({
                projects: this.state.projects.reverse(),
                direction: this.state.direction === 'ascending' ? 'descending' : 'ascending',
            })
        }

    }

    handleSearchValue(e) {
        this.setState({valueSearch: e.target.value})
    }

    // Search bar in table header
    projectSearch(e) {
        this.props.searchProject(this.state.valueSearch)
    }

    resetSearch() {
        this.setState({valueSearch: ''})
        this.props.resetSearch()
    }

    // Change page on Project Table
    changePageProject(e, activePage) {
        this.props.changePage(activePage)
    }

    // Toggle the display list filter
    handleDisplayFilter() {
        this.setState({displayFilter: !this.state.displayFilter})
    }

    // Toggle radio filter
    handleRadioFilter(mode, e) {
        this.props.changeFilterMode(mode)
    }

    // Call the props for delete the current project
    confirmDeleteProject(id, e) {
        this.props.confirm(id)
    }

    // MAIN RENDER
    render() {
        const { t } = this.props
        const numColumnsTable = 8
        
        // body table for the projects
        let allProjects = this.state.projects
        allProjects = allProjects.map((item, index) => {
            let contentShare = ''
            if (item.proj_share > 0) {
                contentShare = t("MAIN_TBL_SHARED")
            } else {
                contentShare = item.proj_share === -1 ? t("MAIN_TBL_PRIVATE") : t("MAIN_TBL_PUBLIC")
            }

            return (
                <Table.Row key={index}>
                    <Table.Cell collapsing>{item.proj_id}</Table.Cell>
                    <Table.Cell><Link to={'/project/' + item.proj_id}>{item.proj_name}</Link></Table.Cell>
                    <Table.Cell collapsing>{item.updated_at}</Table.Cell>
                    <Table.Cell collapsing textAlign='center'>{item.proj_blocked === 1 ? t("MAIN_TBL_YES") : t("MAIN_TBL_NO")}</Table.Cell>
                    <Table.Cell collapsing>{contentShare}</Table.Cell>
                    <Table.Cell collapsing textAlign='center'>{item.user_name}</Table.Cell>
                    <Table.Cell collapsing textAlign='center'>{item.team_name}</Table.Cell>
                    <Table.Cell collapsing textAlign='right' className='fix-display'>
                        <Icon name='clone'
                            size='big'
                            className='icon-pointer'
                            onClick={this.props.duplicateProject.bind(this, item)}
                        />
                        <NewProjectForm
                            className='icon-pointer'
                            size='big'
                            edit='icon'
                            data={item}
                            optionsProfiles={this.props.optionsProfiles}
                            optionsLayouts={this.props.optionsLayouts}
                            disabled={this.props.user.id !== item.proj_owner ? true : false}
                        />
                        <Can perform='can_delete_project' or='can_manage_project'>
                            <Icon name='trash' color='red' size='big'
                                className='icon-pointer'
                                disabled={this.props.user.id !== item.proj_owner ? true : false}
                                onClick={this.confirmDeleteProject.bind(this, item.proj_id)}
                            />
                        </Can>
                    </Table.Cell>
                </Table.Row>
            )
        })

        // Loader for the project table
        let loadingTableBody = <Table.Row>
                <Table.HeaderCell colSpan={numColumnsTable}>
                        <Loader active inline='centered' size='massive' />
                </Table.HeaderCell>
            </Table.Row>

        return (
            <Table celled striped color='blue' sortable>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell colSpan={numColumnsTable}>
                            <Input type='text' placeholder={t("BTN_SEARCH") + '...'} action>
                                <input
                                    onChange={this.handleSearchValue.bind(this)}
                                    value={this.state.valueSearch}
                                    onKeyDown={(e) => e.key === 'Enter' ? this.projectSearch() : null}
                                />
                            <Button onClick={this.projectSearch.bind(this)}>{t("BTN_SEARCH")}</Button>
                            <Button onClick={this.resetSearch.bind(this)}>{t("BTN_RESET")}</Button>
                            </Input>
                            <Can perform='can_show_filter'>
                                <Button icon
                                    labelPosition='right'
                                    floated='right'
                                    color='blue'
                                    onClick={this.handleDisplayFilter.bind(this)}
                                    >
                                    {t("FLT_FILTER")}
                                    <Icon name='filter' />
                                </Button>
                            </Can>
                        </Table.HeaderCell>
                    </Table.Row>
                    <Table.Row style={{'display': this.state.displayFilter ? 'table-row' : 'none'}}>
                        <Table.HeaderCell colSpan={numColumnsTable}>
                            <List>
                                <List.Item>
                                    <Radio label={t("FLT_PRIVATE")}
                                        checked={this.props.checkedRadioFilter === 'private'}
                                        onChange={this.handleRadioFilter.bind(this, 'private')}
                                    />
                                </List.Item>
                                <List.Item>
                                    <Radio label={t("FLT_TEAM")}
                                        checked={this.props.checkedRadioFilter === 'team'}
                                        onChange={this.handleRadioFilter.bind(this, 'team')}
                                    />
                                </List.Item>
                                <List.Item>
                                    <Radio label={t("FLT_SHARED")}
                                        checked={this.props.checkedRadioFilter === 'shared'}
                                        onChange={this.handleRadioFilter.bind(this, 'shared')}
                                        />
                                </List.Item>
                           </List>
                        </Table.HeaderCell>
                    </Table.Row>
                    <Table.Row>
                        <Table.HeaderCell
                            sorted={this.state.column === 'proj_id' ? this.state.direction : null}
                            onClick={this.handleSort.bind(this, 'proj_id')}
                        >
                            ID
                        </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={this.state.column === 'proj_name' ? this.state.direction : null}
                            onClick={this.handleSort.bind(this, 'proj_name')}
                        >
                            {t("MAIN_TBL_NAME")}
                        </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={this.state.column === 'updated_at' ? this.state.direction : null}
                            onClick={this.handleSort.bind(this, 'updated_at')}
                        >
                            {t("MAIN_TBL_DATEUPDATE")}
                        </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={this.state.column === 'proj_blocked' ? this.state.direction : null}
                            onClick={this.handleSort.bind(this, 'proj_blocked')}
                        >
                            {t("MAIN_TBL_PRJBLOCKED")}
                        </Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={this.state.column === 'proj_share' ? this.state.direction : null}
                            onClick={this.handleSort.bind(this, 'proj_share')}
                        >
                            {t("MAIN_TBL_SHARE")}
                        </Table.HeaderCell>
                        <Table.HeaderCell>{t("MAIN_TBL_USEROWN")}</Table.HeaderCell>
                        <Table.HeaderCell
                            sorted={this.state.column === 'team_name' ? this.state.direction : null}
                            onClick={this.handleSort.bind(this, 'team_name')}
                        >
                            {t("MAIN_TBL_TEAMOWN")}
                        </Table.HeaderCell>
                        <Table.HeaderCell>{t("MAIN_TBL_ACTIONS")}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {this.props.loading ? loadingTableBody : allProjects}
                </Table.Body>

                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell colSpan={numColumnsTable}>
                            <Pagination
                                defaultActivePage={this.state.page}
                                totalPages={this.state.totalPages}
                                onPageChange={this.changePageProject.bind(this)}
                            />
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        )
    }
}

HomepageProjectTable.propTypes = {
    changeFilterMode: PropTypes.func.isRequired,
    confirm: PropTypes.func.isRequired,
    changePage: PropTypes.func.isRequired,
    searchProject: PropTypes.func.isRequired,
    duplicateProject: PropTypes.func.isRequired,
    resetSearch: PropTypes.func.isRequired,
    project: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
    page: PropTypes.number.isRequired,
    loading: PropTypes.bool,
    user: PropTypes.object.isRequired,
    teamID: PropTypes.number.isRequired,
    optionsLayouts: PropTypes.array,
    optionsProfiles: PropTypes.array,
    checkedRadioFilter: PropTypes.string.isRequired
}

export default translate('translations')(HomepageProjectTable)
