import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Form, Segment, Grid, Message } from 'semantic-ui-react'

import ContactForm from './ContactForm'
// import logo from '../assets/simcaa.png'

class LoginForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: '',
            password: '',
        }
    }

    changeFormEmail(event, input) {
        this.setState({user: input.value})
    }

    changeFormPassword(event, input) {
        this.setState({password: input.value})
    }

    callLogin() {
        this.props.login(this.state.user, this.state.password)
    }

    // TODO: DELETE THIS WHEN MESSAGE BECOME USELESS
    componentDidMount() {
        let blueContent = document.getElementById('blue_content')
        let violetContent = document.getElementById('violet_content')
        blueContent.innerHTML = window.env.blue_text_content
        violetContent.innerHTML = window.env.violet_text_content
    }

    render() {
        let disable
        if (this.props.message.view) {
            disable = false
        } else if (!this.props.message.view && this.props.loading) {
            disable = true
        }

        // TODO: DELETE WHEN MESSAGE NOT NEEDED
        let style
        if (this.props.noMessage) {
            style = {'display': 'none'}
        } else {
            style = {'maxWidth': '50%', 'left': '50%', 'transform': 'translateX(-50%)'}
        }

        return (
            <div>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Form size='large' id='login' error={this.props.message.view}>
                            <Segment raised color='green'>
                                {
                                    // TODO: DELETE WHEN LOGO WORKS
                                    // <Message
                                    //     error={this.props.message.view}
                                    //     header={this.props.message.header}
                                    //     content={this.props.message.text}
                                    //     />
                                }
                                <Message error={this.props.message.view}>
                                    <Message.Header>{this.props.message.header}</Message.Header>
                                    {
                                        // <Image size='tiny' src={logo} />
                                    }
                                    {this.props.message.text}
                                </Message>
                                <Form.Input
                                    fluid
                                    icon='user'
                                    iconPosition='left'
                                    placeholder='Username'
                                    value={this.state.user}
                                    onChange={this.changeFormEmail.bind(this)}
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                    value={this.state.password}
                                    onChange={this.changeFormPassword.bind(this)}
                                />
                                <Button type='submit' fluid color='blue'
                                    disabled={disable}
                                    loading={disable}
                                    onClick={this.callLogin.bind(this)}>Login</Button>
                            </Segment>
                        </Form>
                    <ContactForm style={{marginTop: '10px'}} />
                    </Grid.Column>
                </Grid>
                <Segment style={style}>
                    <Message
                        color='blue'
                        size='massive'
                    >
                    <Message.Header>
                        {window.env.blue_text_header}
                    </Message.Header>
                    <Message.Content id='blue_content'>
                    </Message.Content>
                    </Message>
                    <Message
                        color='purple'
                        size='massive'
                    >
                    <Message.Header>{window.env.violet_text_header}</Message.Header>
                    <Message.Content id='violet_content'>
                    </Message.Content>
                    </Message>
                </Segment>

            </div>
        )
    }

}

LoginForm.propTypes = {
    login: PropTypes.func.isRequired,
    message: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
}

export default LoginForm