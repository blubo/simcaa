import React, { Component } from 'react'
import { Button, Input, Icon } from 'semantic-ui-react'

export default class Captcha extends Component {
	constructor() {
		super()
		this.state = {
			code: '',
			verify: '',
		}
	}

	componentDidMount() {
		this.createCaptcha()
	}

	createCaptcha() {
		let code = this.state.code
		//clear the contents of captcha div first 
		document.getElementById('captcha').innerHTML = ""
		let charsArray = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*"
		let lengthOtp = 6
		let captcha = []
		for (let i = 0; i < lengthOtp; i++) {
			//below code will not allow Repetition of Characters
			let index = Math.floor(Math.random() * charsArray.length + 1) //get the next character from the array
			if (captcha.indexOf(charsArray[index]) == -1) {
				captcha.push(charsArray[index])
			} else {
				i--
			}
		}
		let canv = document.createElement("canvas")
		canv.id = "captcha"
		canv.width = 200
		canv.height = 50
		let ctx = canv.getContext("2d")
		ctx.font = "25px Georgia"
		ctx.strokeText(captcha.join(""), 0, 30)
		//storing captcha so that can validate you can save it somewhere else according to your specific requirements
		code = captcha.join("")
		document.getElementById("captcha").appendChild(canv) // adds the canvas to the body element
		this.setState({code})
	}

	validateCaptcha(e) {
		e.preventDefault()
		if (document.getElementById("cpatchaTextBox").value === this.state.code) {
			this.setState({verify: true})
			if (this.props.validationPassedCallback) {
				this.props.validationPassedCallback()
			}
		} else {
			this.setState({verify: false})
			if (this.props.validationFailedCallback) {
				this.props.validationFailedCallback()
			}
			this.createCaptcha()
		}
	}

	render() {
		let verifiedIcon = ''
		switch (this.state.verify) {
			case '':
				verifiedIcon = ''
				break
			case true:
				verifiedIcon = <Icon name='check' circular inverted color='green' onClick={this.createCaptcha.bind(this)} />
				break
			case false:
				verifiedIcon = <Icon name='times' circular inverted color='red' onClick={this.createCaptcha.bind(this)} />
				break
			default:
				break
		}

		return (
			<form onSubmit={this.validateCaptcha.bind(this)}>
				<div id="captcha" />
				{verifiedIcon}
				<Input action>
					<Input
						disabled={typeof this.state.verify === "boolean" ? this.state.verify : false}
						placeholder="Captcha" id="cpatchaTextBox"
						icon={<Icon name='refresh' inverted circular link onClick={this.createCaptcha.bind(this)} />}
					/>
					<Button type='submit' disabled={typeof this.state.verify === "boolean" ? this.state.verify : false}>Verify</Button>
  				</Input>
			</form>
		)
  	}
}