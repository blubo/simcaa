import React, { Component } from 'react'
import { Container, Table, Button, Image, Grid } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import UploadSymbol from '../../containers/UploadSymbol'

class PreloadTable extends Component {
    // Handle name image (TODO: verify if needed)
    // fixWidthItemImg(id, value) {
    //     let divContainerWidth = (65 /100) * window.innerWidth
    //     let runFunction = false // esegui la funzione solo se entra nel ifelse
    //
    //     if(divContainerWidth < 800 && divContainerWidth >= 350) {
    //         runFunction = true
    //         value = value.match(new RegExp('.{1,' + value.length / 2 + '}', 'g'))
    //     } else if(divContainerWidth < 350) {
    //         runFunction = true
    //         value = value.match(new RegExp('.{1,' + value.length / 4 + '}', 'g'))
    //     }
    //
    //     if(runFunction) {
    //         let newValue = ''
    //         for(let i = 0; i < value.length; i++) {
    //             newValue += value[i]
    //             if(i < divContainerWidth) {
    //                newValue += '\n'
    //             }
    //         }
    //         return newValue
    //     }
    //
    //     return value
    // }

    // MAIN RENDER
    render() {
        const { t } = this.props

        let localData = this.props.data.map((item, index) => {
            let imgsrc = item.with_image === 1 ? window.env.CustomImage + item.symbol_sign : window.env.PathImages + item.symbol_sign

            // Adjust the item to match the cardArray
            item.custom = item.with_image === 1 ? true : false
            item.img = item.symbol_sign
            delete item['symbol_sign']
            item.lemma = item.voice_human

            return (
                <Grid.Row key={index}>
                    <Grid.Column style={{textAlign: 'left'}} width='12' id={item.id}>
                        <Table compact celled color={"blue"}>
                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell>Id:</Table.Cell>
                                    <Table.Cell>{item.id}</Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>{t("PRLD_TBL_WORD")}:</Table.Cell>
                                    <Table.Cell>{item.voice_human}</Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>{t("PRLD_TBL_FILENAME")}:</Table.Cell>
                                    <Table.Cell><font>{item.img}</font></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>{t("PRLD_TBL_ORIGIN")}:</Table.Cell>
                                    <Table.Cell><font>{item.origin}</font></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>{t("PRLD_TBL_CREATEDBY")}:</Table.Cell>
                                    <Table.Cell>{item.username}</Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>{t("PRLD_TBL_TEAM")}:</Table.Cell>
                                    <Table.Cell>{item.team_name}</Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell>{t("PRLD_TBL_STAFF")}:</Table.Cell>
                                    <Table.Cell>{item.staff_name}</Table.Cell>
                                </Table.Row>
                            </Table.Body>
                        </Table>
                    </Grid.Column>
                    <Grid.Column width='4' style={{position: 'relative'}}>
                        <Image src={imgsrc} size='small' key={index} centered/>
                        <Button.Group className='preload-manage-button'>
                            <UploadSymbol
                                type='button'
                                buttonTranslateText='UPSY_EDIT'
                                buttonActionTranslateText='UPSY_EDIT'
                                buttonColor='green'
                                mode='edit'
                                style={{flex: 'auto'}}
                                card={item}
                                refresh={() => this.props.refresh()}
                            />
                            <Button.Or />
                            <UploadSymbol
                                type='button'
                                buttonTranslateText='UPSY_DELETE'
                                buttonActionTranslateText='UPSY_DELETE'
                                buttonColor='red'
                                mode='delete'
                                style={{flex: 'auto'}}
                                card={item}
                                refresh={() => this.props.refresh()}
                            />
                    </Button.Group>
                    </Grid.Column>
                </Grid.Row>
            )
        })

        // MAIN RENDER RETURN
        return (
            <Container style={{padding: '10px'}} textAlign='center'>
                <Grid celled style={{position: "relative"}}>
                    {localData}
                </Grid>
            </Container>
        )
    }
}

export default translate('translations')(PreloadTable)
