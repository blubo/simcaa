import React, { Component, Fragment } from 'react'
import { Card, Input, Image } from 'semantic-ui-react'

class CardLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            card: [{id:0 ,
                    lemma: 'React',
                    lemmaPrevious: 'React',
                    img: 'react.png',
                    sinonimi: 0,
                    imgAlt: [{voice_human: 'react',voice_start: 'react', voice_last: 'react', img: 'react.png'}],
                    lock: 'unlock',
                    codClass: 'Altro',
                    complex: 0,
                }],
            borderCard: {},
        }
    }

    componentDidMount() {
        this.checkPropsColorCard()
        let styleCard = this.props.borderCard ?
        {
            Aggettivo: {border: this.props.borderCard['Aggettivo'].size + 'px ' + this.props.borderCard['Aggettivo'].type + ' ' + this.props.borderCard['Aggettivo'].color},
            Articolo: {border: this.props.borderCard['Articolo'].size + 'px ' + this.props.borderCard['Articolo'].type + ' ' + this.props.borderCard['Articolo'].color},
            Avverbio: {border: this.props.borderCard['Avverbio'].size + 'px ' + this.props.borderCard['Avverbio'].type + ' ' + this.props.borderCard['Avverbio'].color},
            Congiunzione: {border: this.props.borderCard['Congiunzione'].size + 'px ' + this.props.borderCard['Congiunzione'].type + ' ' + this.props.borderCard['Congiunzione'].color},
            Interiezione: {border: this.props.borderCard['Interiezione'].size + 'px ' + this.props.borderCard['Interiezione'].type + ' ' + this.props.borderCard['Interiezione'].color},
            Pronome: {border: this.props.borderCard['Pronome'].size + 'px ' + this.props.borderCard['Pronome'].type + ' ' + this.props.borderCard['Pronome'].color},
            Preposizione: {border: this.props.borderCard['Preposizione'].size + 'px ' + this.props.borderCard['Preposizione'].type + ' ' + this.props.borderCard['Preposizione'].color},
            Sostantivo: {border: this.props.borderCard['Sostantivo'].size + 'px ' + this.props.borderCard['Sostantivo'].type + ' ' + this.props.borderCard['Sostantivo'].color},
            Verbo: {border: this.props.borderCard['Verbo'].size + 'px ' + this.props.borderCard['Verbo'].type + ' ' + this.props.borderCard['Verbo'].color},
            Altro: {border: this.props.borderCard['Altro'].size + 'px ' + this.props.borderCard['Altro'].type + ' ' + this.props.borderCard['Altro'].color}
        } : {}
        this.setState({borderCard: styleCard})
    }

    componentWillMount() {
        if (this.checkPropsCard() === true) {
            this.setState({card: this.props.Card})
        }
    }

    componentDidUpdate(prevProps) {
        this.checkPropsColorCard()
        if (prevProps.Card && this.props.Card && prevProps.Card !== this.props.Card) {
            this.setState({card: this.props.Card})
        }
        
    }

    // Setto il colore delle input in caso di profilo non base
    checkPropsColorCard() {
        let cardElement = document.getElementById('textLayout-' + this.state.card.id)
        if (cardElement) {
            cardElement.style.setProperty('color', this.props.colorTextInput, 'important')
            cardElement.style.setProperty('background-color', this.props.colorBackgroundInput, 'important')
        }
    }

    checkPropsCard() {
        if (this.props.Card) {
            return true
        }
        else {
            return false
        }
    }

    // Get watermark image name
    getWatermarkSrc(watermark) {
        let index = this.props.watermark.findIndex(x => x.namewater === 'wm:' + watermark)
        return window.env.WaterImages + this.props.watermark[index].filewater
    }

    render() {
        let inputBorder = this.props.transparent === 'normal' ? false : true
        let imgFullWidth = this.props.imgFullWidth ? 'imgFullWidth' : ''

        let srcImg = ''
        if (this.props.Card.custom !== undefined && this.props.Card.custom === true) {
            srcImg = window.env.CustomImage + this.state.card.img
        } else if (this.props.urlImg) {
            srcImg = this.props.urlImg + this.state.card.img
        } else {
            srcImg = window.env.PathImages + this.state.card.img
        }

        let styles = {}
        if (this.props.Style && this.props.borderCard) {
            styles = Object.assign({}, this.props.Style, this.state.borderCard[this.state.card.codClass]);
        }
        else if (this.props.Style) {
            let {top, ...other} = this.props.Style
            styles = other
        }
        else if (this.props.borderCard) {
            styles = this.state.borderCard[this.state.card.codClass]
        }

        let classNameInput
        if (this.state.card.water) {
            classNameInput = this.props.formatInput + ' ' + this.props.weightInput + ' ' + this.props.decorationInput + ' ' + this.props.fontStyleInput + ' colorTextInput colorBackgroundInput input-water'
        } else {
            classNameInput = this.props.formatInput + ' ' + this.props.weightInput + ' ' + this.props.decorationInput + ' ' + this.props.fontStyleInput + ' colorTextInput colorBackgroundInput'
        }

        let cardInput = <Card.Content>
                            <Input
                                size= {this.props.sizeInput}
                                disabled={this.props.mode}
                                transparent={inputBorder}
                                className={this.state.card.hideInput ? `input-water ${classNameInput}` : classNameInput}
                                id = {'textLayout-' + this.state.card.id}
                                value = {this.state.card.lemma}
                            />
                        </Card.Content>

        // let watermarkStyle = this.props.posInput === 'top' ? {bottom: '0px'} : {top: '0px'}
        let cardImage = <Card.Content className={`position-relative ${this.props.imgPadding}`}>
                            <Fragment>
                                <Image
                                    src={this.state.card.water ? window.env.WaterImages + this.state.card.img : srcImg}
                                    className={imgFullWidth}
                                    size={this.props.imgSize}
                                />
                                {this.state.card.watermark && this.state.card.watermark.negazione ? <Image src={this.getWatermarkSrc('negazione')} size={this.props.imgSize} className={`watermark-negated ${imgFullWidth}`}/> : null}
                                {this.state.card.watermark && this.state.card.watermark.plurale ? <Image src={this.getWatermarkSrc('plurale')} size={this.props.imgSize} className={`watermark-all ${imgFullWidth}`}/> : null}
                                {this.state.card.watermark && this.state.card.watermark.passato ? <Image src={this.getWatermarkSrc('passato')} size={this.props.imgSize} className={`watermark-all ${imgFullWidth}`}/> : null}
                                {this.state.card.watermark && this.state.card.watermark.futuro ? <Image src={this.getWatermarkSrc('futuro')} size={this.props.imgSize} className={`watermark-all ${imgFullWidth}`}/> : null}
                            </Fragment>
                        </Card.Content>

        let classNameCard = this.state.card.water ? 'uicardlayout uicardlayout-water' : 'uicardlayout'
        // if (this.state.card.hideShadow && !document.getElementById(`layout-${this.state.card.id}`).classList.contains('multipleDrag')) {
        //     classNameCard = 'no-box-shadow'
        // }

        if (this.state.card.hideShadow && !this.state.card.multipleDrag) {
            classNameCard = classNameCard + ' ' + 'no-box-shadow'
        }

        if (this.state.card.multipleDrag) {
            classNameCard = classNameCard + ' ' + 'multipleDrag'
        }



        if (this.props.posInput === 'bottom' && this.props.isTypo === true) {
            return(
                <Card
                    id={this.props.isTitle ? null : 'layout-' + this.state.card.id}
                    style={{...styles}}
                    className={classNameCard}
                    onClick={this.props.onClick}
                >
                    {cardImage}
                    {cardInput}
                </Card>
            )
        }
        else if (this.props.posInput === 'top' && this.props.isTypo === true) {
            return(
                <Card
                    id={this.props.isTitle ? null : 'layout-' + this.state.card.id}
                    style={{...styles}}
                    className={classNameCard}
                    onClick={this.props.onClick}
                >
                    {cardInput}
                    {cardImage}
                </Card>
            )
        }
        else if (this.props.posInput === 'bottom' && this.props.isTypo === false) {
            return(
                <Card.Group>
                    <div className={this.props.disabledCard}>
                        <Card
                            id={'layout-' + this.state.card.id}
                            style={{...styles}}
                            className='cardUI'
                        >
                            {cardImage}
                            {cardInput}
                        </Card>
                    </div>
                </Card.Group>
            )
        }
        else if (this.props.posInput === 'top' && this.props.isTypo === false) {
            return(
                <Card.Group>
                    <div className={this.props.disabledCard}>
                        <Card
                            id={'layout-' + this.state.card.id}
                            style={{...styles}}
                            className="cardUI"
                        >
                            {cardInput}
                            {cardImage}
                        </Card>
                    </div>
                </Card.Group>
            )
        }
    }
}

export default CardLayout
