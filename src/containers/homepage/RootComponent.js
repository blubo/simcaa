import React, { Component } from 'react';
import { Container, Grid, Segment, Header, Confirm, Loader, Dimmer } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import { translate } from 'react-i18next'
import NewProjectForm from './NewProjectForm'

import HomepageNavbar from '../../components/homepage/HomepageNavbar'
import HomepageProjectTable from '../../components/homepage/HomepageProjectTable'
import HomepageLayoutTable from '../../components/homepage/HomepageLayoutTable'

import { connect } from 'react-redux'
import { projectDataFetchData, projectDataDeleteChapterData, duplicateProject } from '../../store/actions/ProjectFetchActions'
import { profileDataDeleteData } from '../../store/actions/ProfileFetchActions'
import { dispatchFilter, setPaginationSelectedPage } from '../../store/actions/FilterActions'
import Can from '../Permission'

class RootComponent extends Component {
    constructor(props) {
        super(props)
        this.state= {
            profiles: props.profile,
            layouts: props.layout,
            openConfirmProject: false,
            openConfirmProfile: false,
            openConfirmDuplicate: false,
            deleteProjectId: 0,
            deleteProfileId: 0,
            duplicateProjectId: 0,
            optionsProfiles: [{}],
            optionsLayouts: [{}],
            fetchFinished: false,
            radioFilter: props.filterMode,
            page: props.projectPage,
        }
    }

    // Richiama i profili e i progetti pubblici o dell'utente
    componentWillMount() {
        // Set Profile for Modal
        // let localOptionsProfile = this.state.optionsProfiles
        // for (var i = 0; i < this.state.profiles.length; i++) {
        //     localOptionsProfile[i] = {value: this.state.profiles[i].profile_conf,
        //                                 text: this.state.profiles[i].profile_name + ' (' + this.state.profiles[i].user_name + ')'}
        // }
        //
        // // Set Layout for Modal
        // // let localOptionsLayout = this.state.optionsLayouts
        // for (var i = 0; i < this.state.layouts.length; i++) {
        //     localOptionsLayout[i] = {value: JSON.stringify(this.state.layouts[i]),
        //                                 text: this.state.layouts[i].layout_name}
        // }

        let localOptionsProfile = this.props.profile.slice()
        for (let i = 0; i < this.props.profile.length; i++) {
            localOptionsProfile[i] = {value: this.props.profile[i].profile_conf,
                                        text: this.props.profile[i].profile_name + ' (' + this.props.profile[i].user_name + ')'}
        }

        // Set Layout for Modal
        // let localOptionsLayout = this.props.optionsLayouts
        let localOptionsLayout = this.props.layout.slice()
        for (let i = 0; i < this.props.layout.length; i++) {
            localOptionsLayout[i] = {value: JSON.stringify(this.props.layout[i]),
                                        text: this.props.layout[i].layout_name}
        }

        this.setState({optionsProfiles: localOptionsProfile,
                        optionsLayouts: localOptionsLayout,
                        fetchFinished: true})
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.componentWillMount()
        }
    }

    // Toggle radio filter
    handleRadioFilter(mode, e) {
        this.setState({radioFilter: mode })
        this.props.changeFilter(mode, null)
        this.props.fetchProject(null, 15, 1)
    }

    // Apre il popup per confermare l'eliminazione del progetto
    handleOpenConfirmProject(id, event) {
        // Controlla se l'utente è effetivamente il proprietario del progetto
        if (this.props.project.find(x => x.proj_id === id).proj_owner === this.props.user.id) {
            this.setState({openConfirmProject: true, deleteProjectId: id})
        }
    }

    // Apre il popup per confermare l'eliminazione del profilo
    handleOpenConfirmProfile(id, event) {
        // Controlla se l'utente è effetivamente il proprietario del profilo
        if (this.props.profile.find(x => x.id === id).profile_user_id === this.props.user.id) {
            this.setState({openConfirmProfile: true, deleteProfileId: id})
        }
    }

    // Apre il popup per confermare l'eliminazione del progetto
    handleOpenConfirmDuplicate(id, event) {
        this.setState({openConfirmDuplicate: true, duplicateProjectId: id})
    }

    // Chiude il popup se si preme il tasto cancella
    handleCancel() {
        this.setState({openConfirmProject: false, openConfirmProfile: false, openConfirmDuplicate: false})
    }

    // Conferma ed elimina il progetto
    handleConfirmProject() {
        this.setState({openConfirmProject: false})
        this.deleteProject()
    }

    // Conferma ed elimina il profilo
    handleConfirmProfile() {
        this.setState({openConfirmProfile: false})
        this.deleteProfile()
    }

    // Conferma e duplica il progetto
    handleConfirmDuplicate() {
        this.setState({openConfirmDuplicate: false})
        this.props.duplicateProject(this.state.duplicateProjectId)
    }

    // Elimina il progetto e tutti i capitoli ad esso correlati
    deleteProject() {
        this.props.deleteCurrentProject(this.state.deleteProjectId, true)
    }

    // Elimina il profilo
    deleteProfile() {
        this.props.deleteCurrentProfile(this.state.deleteProfileId)
    }

    // Change page on Project Table
    changePageProject(activePage) {
        this.setState({page: activePage.activePage}, () => {
            this.props.changePage(this.state.page)
            this.props.fetchProject(null, 15, this.state.page)
        })
    }

    // Search project
    searchProject(value) {
        this.props.fetchProject(null, 15, 1, value)
    }

    // Reset search
    resetSearch() {
        // this.props.fetchProject(null, 15, 1)
        this.changePageProject({activePage: 1})
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        let dropdownTeamOptions = []
        this.props.userTeam.forEach((item) => {
            dropdownTeamOptions.push({'text': item.team_name, 'value': item.team_id})
        })

        // Renderizza il Loader se non ha ancora finito le chiamate al db
        if (!this.state.fetchFinished) {
            return (
                <Dimmer
                    active={!this.state.fetchFinished}
                    page
                >
                    <Loader active inline='centered' size='massive' />
                </Dimmer>
            )
        }

        return (
            <Container>
                <HomepageNavbar
                    user={this.props.user}
                    resetStore={this.props.resetStore}
                />
                <Grid columns='equal' style={{padding: '10px'}}>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <Header size='large' className='fix-display' style={{'display': 'inline'}}>
                                    <Can perform='can_manage_project' or='can_create_project'>
                                        <NewProjectForm
                                            className='icon-pointer'
                                            size='big'
                                            optionsProfiles={this.state.optionsProfiles}
                                            optionsLayouts={this.state.optionsLayouts}
                                        />
                                    </Can>
                                    <Header.Content>
                                        {t("MAIN_LBL_ALL")}
                                        <Header.Subheader>
                                            {t("MAIN_LBL_ALL")}
                                        </Header.Subheader>
                                    </Header.Content>
                                </Header>
                                <HomepageProjectTable
                                    changeFilterMode={this.handleRadioFilter.bind(this)}
                                    confirm={this.handleOpenConfirmProject.bind(this)}
                                    changePage={this.changePageProject.bind(this)}
                                    searchProject={this.searchProject.bind(this)}
                                    duplicateProject={this.handleOpenConfirmDuplicate.bind(this)}
                                    resetSearch={this.resetSearch.bind(this)}
                                    total={this.props.projectTotal}
                                    page={this.state.page}
                                    project={this.props.project}
                                    loading={this.props.projectLoading}
                                    user={this.props.user}
                                    teamID={this.props.filterValue}
                                    optionsLayouts={this.state.optionsLayouts}
                                    optionsProfiles={this.state.optionsProfiles}
                                    checkedRadioFilter={this.state.radioFilter}
                                />
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment>
                                <Header size='large'>
                                    <Header.Content>
                                        {t("MAIN_LBL_ALLPROFILE")}
                                        <Header.Subheader>
                                            {t("MAIN_LBL_MANAGEPROFILE")}
                                        </Header.Subheader>
                                    </Header.Content>
                                </Header>
                                <HomepageLayoutTable
                                    user={this.props.user}
                                    profile={this.props.profile}
                                    confirm={this.handleOpenConfirmProfile.bind(this)}
                                />
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Confirm
                    open={this.state.openConfirmProject}
                    header={t("DELETE_CNF_H")}
                    content={t("DELETE_CNF_P")}
                    confirmButton={t("BTN_CONFIRM")}
                    cancelButton={t("BTN_DENY")}
                    onCancel={this.handleCancel.bind(this)}
                    onConfirm={this.handleConfirmProject.bind(this)}
                />
                <Confirm
                    open={this.state.openConfirmProfile}
                    header={t("DELETE_CNF_H")}
                    content={t("DELETE_CNF_C")}
                    confirmButton={t("BTN_CONFIRM")}
                    cancelButton={t("BTN_DENY")}
                    onCancel={this.handleCancel.bind(this)}
                    onConfirm={this.handleConfirmProfile.bind(this)}
                />
                <Confirm
                    open={this.state.openConfirmDuplicate}
                    header={t("MAIN_CNF_DUPLICATE_H")}
                    content={t("MAIN_CNF_DUPLICATE_C")}
                    confirmButton={t("BTN_CONFIRM")}
                    cancelButton={t("BTN_DENY")}
                    onCancel={this.handleCancel.bind(this)}
                    onConfirm={this.handleConfirmDuplicate.bind(this)}
                />
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        project: state.projectData,
        projectTotal: state.projectDataTotal,
        projectPage: state.projectPaginationPage,
        projectLoading: state.projectDataIsLoading,
        profile: state.profileData,
        layout: state.layoutData,
        userTeam: state.userTeamData,
        filterMode: state.filterMode,
        filterValue: state.filterValue,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProject: (query, limit, page, searchValue) => dispatch(projectDataFetchData(query, limit, page, searchValue)),
        deleteCurrentProject: (id, deleteProject) => dispatch(projectDataDeleteChapterData(id, deleteProject)),
        deleteCurrentProfile: (id) => dispatch(profileDataDeleteData(id)),
        changeFilter: (mode, value) => dispatch(dispatchFilter(mode, value)),
        changePage: (value) => dispatch(setPaginationSelectedPage(value)),
        duplicateProject: (id) => dispatch(duplicateProject(id)),
        resetStore: () => dispatch({type: "RESET_STORE", state: undefined}),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(translate('translations')(RootComponent)))
