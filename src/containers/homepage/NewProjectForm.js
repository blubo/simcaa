import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Form, Dropdown, Message, TextArea, Dimmer, Loader, Segment, Input, Label } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import { escapeQuotes, isJsonEmpty } from '../../store/functionUtils'
import { connect } from 'react-redux'
import { projectDataUpdateData, projectDataCreateData, lockUnlockProject } from '../../store/actions/ProjectFetchActions'
import { membersTeamDataFetchData } from '../../store/actions/UserTeamFetchActions'
import Can, { Permission } from '../../containers/Permission.js'
import { apolloFetch } from '../../store/apolloFetchUtils'

class NewProjectForm extends Component {
    constructor(props) {
        super(props)
        this.state= {
            modalVisible: false,
            errorMessage: {hidden: true, text: props.t("PRJ_FRM_ERROR_CONTENT")},
            share: null,
            profile: this.props.edit ? this.props.data.proj_profile : '',
            optionsProfiles: this.props.optionsProfiles,
            newProjectTitle: '',
            newProjectNote: '',
            iconColor: 'black',
            indexDefaultLayout: 0,
            optionsShare: [],
            blockModalOpen: false,
            loading: false,
            searchShare: '',
            shareUser: {},
            selectedSharedUserID: 0
        }
    }

    componentWillMount() {
        if(this.props.edit && this.props.data) {
            let projectTitle = this.props.data.proj_name
            let projectNote = this.props.data.proj_note
            let share = this.props.data.proj_share
            this.setState({newProjectTitle: projectTitle, newProjectNote: projectNote, share})
        }
    }

    componentDidMount() {
        // build Profile options for the dropdown
        if (this.props.edit && this.props.data) {
            let localProfile = this.props.optionsProfiles.slice()
            if (typeof localProfile === 'object') {
                let newElement = {value: this.props.data.proj_profile, text: this.props.t("MAIN_FRM_CURRENTPROFILE")}
                localProfile.unshift(newElement)
                localProfile = localProfile.map((item, index) => {
                    let newProfileArray = Object.assign({}, item)
                    newProfileArray.key = index
                    return newProfileArray
                })
                this.setState({optionsProfiles: localProfile})
            }
        }
        
        this.setShareOptions()
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            let team_id = this.props.data ? this.props.data.team_id : this.props.filterValue
            if (this.props.memberTeam.length > 0 && this.props.memberTeam[0].team_id === team_id) {
                this.setState({loading: false})
            }
            this.componentDidMount()
        }
    }

    // Handle get user team member for the current project
    getUsersTeam() {
        if (Permission(true, this.props.user.user_permissions['can_share_project'])) {
            // let team_id = this.props.data ? this.props.data.team_id : this.props.filterValue
            // if (this.props.memberTeam.length > 0 && this.props.memberTeam[0].team_id !== team_id) {
            //     let team_id = this.props.data ? this.props.data.team_id : this.props.filterValue
            //     this.setState({loading: true})
            //     this.props.getMembersTeam(null, team_id)
            // }
            if (this.props.data && this.props.edit && this.props.data.proj_share > 0) {
                this.searchShare('info')
            }
        }
    }

    // build Team Members/Share options for the dropdown
    setShareOptions() {
        let newMemberTeamArray = []
        // let localMemberTeam = this.props.memberTeam.slice()
        newMemberTeamArray[0] = {text: this.props.t("MAIN_TBL_PUBLIC"), value: -2}
        newMemberTeamArray[1] = {text: this.props.t("MAIN_TBL_PRIVATE"), value: -1}
        
        // if (this.props.filterMode !== 'shared') {
        //     localMemberTeam = localMemberTeam.filter(item => {
        //         if (item.user_id !== this.props.user.id) {
        //             return true
        //         }
        //     })
        // }

        // localMemberTeam = localMemberTeam.map(item => {
        //     return {text: this.props.t("PRJ_FRM_SHARE_WITH") + ' ' + item.username, value: item.user_id}
        // })
        
        // let finalOption = newMemberTeamArray
        if (Permission(true, this.props.user.user_permissions['can_share_project'])) {
            newMemberTeamArray[2] = {text: this.props.t("MAIN_FRM_SHAREUSER"), value: 0}
            // finalOption = newMemberTeamArray.concat(localMemberTeam)
        }
        // this.setState({optionsShare: finalOption})
        this.setState({optionsShare: newMemberTeamArray})
    }

    // Search for user to share project
    searchShare(mode = 'search') {
        let where
        if (mode === 'info' && this.props.data.proj_share > 0) {
            where = `user_id: ${this.props.data.proj_share}`
        } else {
            where = this.state.searchShare.includes('@') ?
                `email: "${this.state.searchShare}", team_id: ${this.props.filterValue}`
                :
                `username: "${this.state.searchShare}", team_id: ${this.props.filterValue}`
        }
        let query = `
        query searchUserShare {
            user_teams(${where}) {
                data {
                    user_id
                    username
                    email
                }
            }
        }
        `
        this.setState({loading: true})
        apolloFetch({query})
            .then(data => {
                this.setState({loading: false})
                if (data.errors) {
                    console.log(data.errors)
                } else {
                    if (data.data.user_teams.data.length > 0) {
                        this.setState({
                            shareUser: data.data.user_teams.data[0],
                            selectedSharedUserID: mode === 'info' ? data.data.user_teams.data[0].user_id : 0
                        })
                    } else {
                        this.setState({shareUser: {},
                            errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_SHARE_USER_NOTFOUND")}
                        })
                    }
                }
            })
            .catch(error => {
                this.setState({loading: false})
                console.log(error)
            })
    }

    // Handle open/close main modal
    handleOpenCloseModal(action) {
        let title, note
        if (action === 'open') {
            if (this.props.edit && this.props.data) {
                title = this.props.data.proj_name
                note = this.props.data.proj_note
            } else {
                title = ''
                note = ''
                this.setState({searchShare: '', shareUser: {}, selectedSharedUserID: 0, share: null})
            }
        } else {
            if (this.props.edit && this.props.data) {
                title = this.props.data.proj_name
                note = this.props.data.proj_note
                let share = this.props.data.proj_share
                this.setState({share})
            } else {
                title = ''
                note = ''
            }
        }

        let localErrorMessage = this.state.errorMessage
        localErrorMessage.hidden = true

        this.setState({modalVisible: !this.state.modalVisible,
                newProjectTitle: title,
                newProjectNote: note,
                errorMessage: localErrorMessage})
    }

    handleBlockModal() {
        this.setState({blockModalOpen: !this.state.blockModalOpen})
    }

    // Handle change name/title project input
    handleFormChange(e) {
        this.setState({newProjectTitle: e.target.value})
    }

    // Handle change Dropdown value by type (profile/layout)
    handleDropdownChange(dropdownType, e, data) {
        if(dropdownType === 'profile') {
            this.setState({profile: data.value})
        }
    }

    // Handle change Share value
    handleChangeShare(e, data) {
        const { value } = data
        if (value === 0 && this.state.share !== 0) {
            this.setState({share: value})
        } else {
            this.setState({share: value, shareUser: {}, selectedSharedUserID: 0, searchShare: ""})
        }
    }

    // Handle change TextArea input value
    handleTextAreaChange(e) {
        this.setState({newProjectNote: e.target.value})
    }

    handleSelectedShareID() {
        if (this.props.user.id === this.state.shareUser.user_id) {
            this.setState({errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_SHARE_USER_SAME")}})
        } else {
            this.setState({selectedSharedUserID: this.state.shareUser.user_id})
        }
    }

    // Call the create function from props
    createNewProject() {
        let localShare = this.state.share === 0 && this.state.selectedSharedUserID !== 0 ? this.state.selectedSharedUserID : this.state.share
        if(this.state.newProjectTitle === '') {
            this.setState({errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_CONTENT")}})
        } else if (typeof this.state.profile !== 'string' || this.state.profile === '') {
            this.setState({errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_PROFILE")}})
        } else if (this.state.share === null) {
           this.setState({errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_SHARE")}})
        } else if (localShare === 0) {
           this.setState({errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_SHARE_USER")}})
        } else {
            this.createProject(this.state.newProjectTitle, this.state.newProjectNote, localShare, this.state.profile)
            this.setState({errorMessage: {hidden: true, text: this.props.t("PRJ_FRM_ERROR_CONTENT")}, modalVisible: !this.state.modalVisible})
        }
    }

    // Crea un nuovo progetto
    createProject(title, notes, share, profile) {
        // Parse all Variables
        let proj_name = escapeQuotes(title)
        let proj_note = escapeQuotes(notes)
        let proj_owner = this.props.user.id
        let proj_share = share
        let proj_profile = escapeQuotes(profile)
        let proj_team = parseInt(this.props.proj_team, 10)
        let proj_blocked = 0

        let query = `
        mutation createProject {
            createCaaProject(proj_name: "${proj_name}",
                            proj_owner: ${proj_owner},
                            proj_share: ${proj_share},
                            proj_profile: "${proj_profile}",
                            proj_blocked: ${proj_blocked},
                            proj_team: ${proj_team},
                            proj_note: "${proj_note}"){
                id
            }
        }
        `
        this.props.createNewProject(query)
    }

    // Call the update function from props
    updateProject() {
        let localShare = this.state.share === 0 && this.state.selectedSharedUserID !== 0 ? this.state.selectedSharedUserID : this.state.share
        if(this.state.newProjectTitle === '') {
            this.setState({errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_CONTENT")}})
        } else if (localShare === 0) {
            this.setState({errorMessage: {hidden: false, text: this.props.t("PRJ_FRM_ERROR_SHARE_USER")}})
        } else {
            this.dispatchUpdateProject(this.props.data.proj_id, this.state.newProjectTitle, this.state.newProjectNote,
                                        localShare, this.state.profile)
            this.setState({errorMessage: {hidden: true, text: this.props.t("PRJ_FRM_ERROR_CONTENT")}, modalVisible: !this.state.modalVisible})
        }
    }

    // Aggiorna un progetto preesistente
    dispatchUpdateProject(id, title, notes, share, profile) {
        // Parse all Variables
        let escapedNotes = escapeQuotes(notes)
        let escapedTitle = escapeQuotes(title)
        let proj_share = share
        let proj_profile = escapeQuotes(profile)

        let query = `
        mutation updateProject {
            updateCaaProject(id: ${id},
                            proj_name: "${escapedTitle}",
                            proj_share: ${proj_share},
                            proj_note: "${escapedNotes}",
                            proj_profile: "${proj_profile}"){
                id
            }
        }
        `
        this.props.updateCurrentProject(query)
    }

    // handle call lock/unlock project action
    handleLockUnlockProject() {
        this.props.handleBlockProject(this.props.data.proj_id, 1-this.props.data.proj_blocked)
    }

    // If component is an Icon, change color onOver it
    onMouseOverIcon() {
        let newColor = this.state.iconColor === 'black' ? 'green' : 'black'
        this.setState({iconColor: newColor})
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        // main render modal trigger
        let modalTrigger
        if (this.props.edit) {
            if (this.props.edit === 'icon') {
                modalTrigger = <Icon name='edit'
                    style={this.props.style}
                    className={this.props.className}
                    size={this.props.size}
                    onClick={this.handleOpenCloseModal.bind(this, 'open')}
                    disabled={this.props.disabled}
                />
            } else if (this.props.edit === 'button') {
                modalTrigger = <Button onClick={this.handleOpenCloseModal.bind(this, 'open')} disabled={this.props.disabled}>
                                {t("PRJ_MNU_OPTIONS")}
                            </Button>
            }
        } else {
            modalTrigger = <Icon name='add square'
                style={this.props.style}
                className={this.props.className}
                size={this.props.size}
                color={this.state.iconColor}
                onMouseOver={this.onMouseOverIcon.bind(this)}
                onMouseOut={this.onMouseOverIcon.bind(this)}
                onClick={this.handleOpenCloseModal.bind(this, 'open')}
                disabled={this.props.disabled}
            />
        }

        // Calcola l'indice giusto per prendere il valore di default nell'array dello share in caso di edit
        // let indexShareArray
        // if (this.state.optionsShare.length > 0 && this.props.data) {
        //     indexShareArray = this.state.optionsShare.findIndex(x => x.value === this.props.data.proj_share)
        // }
        let shareDefaultDropdown = null
        shareDefaultDropdown = this.state.share > 0 ? 0 : shareDefaultDropdown
        shareDefaultDropdown = this.state.share < 0 ? this.state.share : shareDefaultDropdown

        return (
            <Fragment>
                <Modal trigger={modalTrigger} closeOnDimmerClick={false} open={this.state.modalVisible} onOpen={this.getUsersTeam.bind(this)}>
                    <Modal.Header>{this.props.edit ? t("MAIN_BTN_UPDATE") : t("MAIN_LBL_CREATE")}</Modal.Header>
                    <Modal.Content>
                        <Dimmer.Dimmable as={Segment} basic dimmed={this.state.loading}>
                        <Dimmer active={this.state.loading}>
                            <Loader>Loading...</Loader>
                        </Dimmer>
                        <Form style={this.state.loading ? {opacity: 0} : null}>
                            <Form.Field required>
                                <label>{t("MAIN_FRM_TITLE")}</label>
                                <input placeholder={t("MAIN_FRM_TITLE")}
                                    value={this.state.newProjectTitle}
                                    onChange={this.handleFormChange.bind(this)}
                                    maxLength={55}
                                    />
                            </Form.Field>
                            <Form.Field required>
                                <label>{t("MAIN_FRM_PROFILE")}</label>
                                <Dropdown selection
                                    options={this.props.edit ? this.state.optionsProfiles : this.props.optionsProfiles}
                                    placeholder={t("PRJ_FRM_ERROR_PROFILE")}
                                    defaultValue={this.props.edit ? this.state.optionsProfiles[0].value : null}
                                    onChange={this.handleDropdownChange.bind(this, 'profile')}
                                    />
                            </Form.Field>
                            <Form.Field required>
                                <label>{t("MAIN_FRM_SHARE")}</label>
                                <Dropdown selection
                                    options={this.state.optionsShare}
                                    placeholder={t("PRJ_FRM_ERROR_SHARE")}
                                    // defaultValue={this.props.edit && this.state.optionsShare.length > 0 && indexShareArray >= 0 ? this.state.optionsShare[indexShareArray].value : null}
                                    defaultValue={shareDefaultDropdown}
                                    onChange={this.handleChangeShare.bind(this)}
                                />
                            </Form.Field>
                            <Can perform="can_share_project">
                                <Fragment>
                                    <Form.Field disabled={this.state.share === 0 ? false : true} width={8}>
                                        <label>{t("MAIN_FRM_SHARE_LBL_SEARCH")}</label>
                                        <Input 
                                            label={<Button onClick={this.searchShare.bind(this)} primary>{t("BTN_SEARCH")}</Button>}
                                            labelPosition='right'
                                            placeholder='Email...'
                                            value={this.state.searchShare}
                                            onChange={(e) => this.setState({searchShare: e.target.value})}
                                        />
                                    </Form.Field>
                                    <Segment basic hidden={isJsonEmpty(this.state.shareUser)}>
                                        <Label as='a' size='massive'>
                                            <Icon name='user' />
                                            {this.state.shareUser.username}
                                            <Button
                                                color={this.state.selectedSharedUserID === this.state.shareUser.user_id ? "green" : "blue"}
                                                style={{'marginLeft': '10px'}}
                                                onClick={this.handleSelectedShareID.bind(this)}
                                            >
                                                {
                                                    this.state.selectedSharedUserID === this.state.shareUser.user_id ?
                                                        t("MAIN_TBL_SHARED")
                                                    :
                                                        t("MAIN_BTN_SHARE")
                                                }
                                            </Button>
                                            <h3><p>{this.state.shareUser.email}</p></h3>
                                        </Label>
                                    </Segment>
                                </Fragment>
                            </Can>
                            <Form.Field>
                                <label>{t("MAIN_FRM_NOTES")}</label>
                                <TextArea placeholder={t("MAIN_FRM_NOTES")}
                                    value={this.state.newProjectNote}
                                    onChange={this.handleTextAreaChange.bind(this)}
                                />
                            </Form.Field>
                            {
                                this.props.edit && this.props.user.id === this.props.data.proj_owner ?
                                <div style={{textAlign: 'center'}}>
                                    <Button
                                        color={this.props.data.proj_blocked === 0 ? 'red' : 'green'}
                                        onClick={this.handleBlockModal.bind(this)}
                                        >
                                        {this.props.data.proj_blocked === 0 ? t("MAIN_FRM_PRJLOCK") : t("MAIN_FRM_PRJUNLOCK")}
                                    </Button>
                                </div>
                                : null
                            }
                        </Form>
                        <Message
                            hidden={this.state.errorMessage.hidden}
                            error
                            header={t("PRJ_FRM_ERROR_HEADER")}
                            content={this.state.errorMessage.text}
                        />
                        </Dimmer.Dimmable>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button.Group>
                            <Button color='green'
                                disabled={this.state.loading}
                                onClick={this.props.edit ? this.updateProject.bind(this) : this.createNewProject.bind(this)}
                            >
                                {this.props.edit ? t("MAIN_BTN_UPDATE") : t("MAIN_BTN_CREATE")}
                            </Button>
                            <Button.Or />
                            <Button color='red' onClick={this.handleOpenCloseModal.bind(this, 'close')}>
                                {t("MAIN_BTN_CLOSE")}
                            </Button>
                        </Button.Group>
                    </Modal.Actions>
                </Modal>
                <Modal open={this.state.blockModalOpen}>
                    <Modal.Header>{t("MAIN_FRM_PRJLOCK")}</Modal.Header>
                    <Modal.Content>
                        {t("HOWTO_BLOCKPROJECT")}
                    </Modal.Content>
                    <Modal.Actions>
                        <Button positive onClick={this.handleLockUnlockProject.bind(this)}>
                            {this.props.data && this.props.data.proj_blocked === 0 ? t("MAIN_FRM_PRJLOCK") : t("MAIN_FRM_PRJUNLOCK")}
                        </Button>
                        <Button negative onClick={this.handleBlockModal.bind(this)}>{t("MAIN_BTN_CLOSE")}</Button>
                    </Modal.Actions>
                </Modal>
            </Fragment>
        )
    }
}

NewProjectForm.propTypes = {
    data: PropTypes.object,
    style: PropTypes.object,
    className: PropTypes.string,
    size: PropTypes.string,
    color: PropTypes.string,
    edit: PropTypes.string,
    disable: PropTypes.bool,
    optionsProfiles: PropTypes.array,
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        project: state.projectData,
        profile: state.profileData,
        proj_team: state.filterValue,
        memberTeam: state.memberTeamData,
        filterMode: state.filterMode,
        filterValue: state.filterValue,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateCurrentProject: (query) => dispatch(projectDataUpdateData(query)),
        handleBlockProject: (id, value) => dispatch(lockUnlockProject(id, value)),
        createNewProject: (query) => dispatch(projectDataCreateData(query)),
        getMembersTeam: (query, id) => dispatch(membersTeamDataFetchData(query, id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(NewProjectForm))
