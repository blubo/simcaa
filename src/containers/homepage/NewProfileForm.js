import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Form, Message, Dropdown, Tab } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import InputOptions from '../../components/options/InputOptions'
import MarginOptions from '../../components/options/MarginOptions'
import ImageOptions from '../../components/options/ImageOptions'
import CardOptions from '../../components/options/CardOptions'
import ProfileOptions from '../../components/options/ProfileOptions'

import { escapeQuotes, isValidJSON } from '../../store/functionUtils'
import { connect } from 'react-redux'
import { profileDataCreateData, profileDataUpdateData } from '../../store/actions/ProfileFetchActions'

class NewProfileForm extends Component {
    constructor(props) {
        super(props)
        this.state= {
            // If something changes here check the save and update profile functions
            modalVisible: false,
            errorMessage: true,
            share: false,
            indexForm: 0,
            newProfileName: '',
            iconColor: 'black',
            // Card Variables for the Form
            cardStyle: { 'top': '0px', 'width': '100%' },
            posInput: 'bottom',
            sizeInput: 'small',
            styleInput: 'normal',
            formatInput: 'freeInput',
            colorTextInput: '#000000',
            colorBackgroundInput: '#FFFFFF',
            weightInput: 'weightNormalInput',
            decorationInput: 'decorationNormalInput',
            fontStyleInput: 'styleNormalInput',
            imgSize: 'small',
            imgPadding: 'imgpadding0',
            imgType: 0,
            imgStyle: 0,
            priorityOrder: [{text: 'type'},{text: 'color'}],
            borderCard: {
                Aggettivo: {color: '',size: '1',type: ''},
                Articolo: {color: '',size: '1',type: ''},
                Avverbio: {color: '',size: '1',type: ''},
                Congiunzione: {color: '',size: '1',type: ''},
                Interiezione: {color: '',size: '1',type: ''},
                Pronome: {color: '',size: '1',type: ''},
                Preposizione: {color: '',size: '1',type: ''},
                Sostantivo: {color: '',size: '1',type: ''},
                Verbo: {color: '',size: '1',type: ''},
                Altro: {color: '',size: '1',type: ''},
                Tutti: {color: '',size: '1',type: ''}
            },
            queryParams: {
                global: true,
                preload_private: true,
                preload_team: true,
                preload_community: true
            },
            layout: props.optionsLayouts[0],
        }
        // Handle Form Options
        this.handleChangeOption = this.handleChangeOption.bind(this)
        this.handleChangeColorTextInput = this.handleChangeColorTextInput.bind(this)
        this.handleChangeColorBackgroundInput = this.handleChangeColorBackgroundInput.bind(this)
        this.handleChangeColorBorder = this.handleChangeColorBorder.bind(this)
        this.handleChangeBorder = this.handleChangeBorder.bind(this)
        this.handleChangeOrder = this.handleChangeOrder.bind(this)
        this.handleChangeLayout = this.handleChangeLayout.bind(this)
        this.handleChangeQueryParams = this.handleChangeQueryParams.bind(this)
        this.resetBorderAll = this.resetBorderAll.bind(this)
    }

    componentWillMount() {
        if(this.props.edit && this.props.data) {
            let data = JSON.parse(this.props.data)
            this.setState({...data})
        }
    }

    handleOpenCloseModal(action) {
        let indexTeam = this.props.teams.findIndex(x => x.team_id === this.props.teamID)
        let teamName = this.props.teams[indexTeam].team_name
        let name
        if (action === 'open') {
            if (this.props.edit && this.props.data) {
                name = this.state.newProfileName.replace(/\"/g, "")
            } else {
                name = 'Profilo ' + teamName
            }
        } else {
            if (this.props.edit && this.props.data) {
                name = JSON.parse(this.props.data).newProfileName
                let data = JSON.parse(this.props.data)
                this.setState({...data})
            } else {
                name = 'Profilo ' + teamName
            }
        }
        this.setState({modalVisible: !this.state.modalVisible, newProfileName: name, indexForm: 0})
    }

    handleFormChange(e) {
        this.setState({newProfileName: e.target.value})
    }

    handleCheckboxChange(e) {
        this.setState({share: !this.state.share})
    }

    handleTabChange(event, data) {
        this.setState({indexForm: data.activeIndex})
    }

    onMouseOverIcon() {
        let newColor = this.state.iconColor === 'black' ? 'green' : 'black'
        this.setState({iconColor: newColor})
    }

    // Handle Card Form TODO: change to delete the switch
    handleChangeOption(data) {
        switch (data.options[0].type) {
            case 'position':
                this.setState({ posInput: data.value })
                break
            case 'size':
                this.setState({ sizeInput: data.value })
                break
            case 'style':
                this.setState({ styleInput: data.value })
                break
            case 'format':
                this.setState({ formatInput: data.value })
                break
            case 'weight':
                this.setState({ weightInput: data.value })
                break
            case 'decoration':
                this.setState({ decorationInput: data.value })
                break
            case 'font-style':
                this.setState({ fontStyleInput: data.value })
                break
            case 'imgsize':

                // TODO: Rivedere e forse eliminare
                let localStyle = JSON.parse(JSON.stringify(this.state.cardStyle));
                if (data.value === 'mini') {
                    localStyle.width = '50%'
                    // localStyle.width = 'auto'
                } else if (data.value === 'tiny') {
                    // localStyle.width = '70%'
                    localStyle.width = 'auto'
                } else {
                    localStyle.width = 'auto'
                }

                this.setState({ imgSize: data.value, cardStyle: localStyle })
                break
            case 'imgpadding':
                this.setState({ imgPadding: data.value })
                break
            case 'imgtype':
                this.setState({ imgType: data.value })
                break
            case 'imgstyle':
                this.setState({ imgStyle: data.value })
                break
            default:
                break
        }

    }

    handleChangeOrder(order) {
        this.setState({priorityOrder: order})
    }

    handleChangeLayout(layout) {
        this.setState({layout})
    }

    handleChangeBorder(data, item) {
        let localBorderCard = this.state.borderCard
        if (item.text === 'Tutti') {
            for (let index in localBorderCard) {
                data.options[0].type === 'bordertype' ?
                localBorderCard[index].type = data.value :
                localBorderCard[index].size = data.value
            }
        } else {
            data.options[0].type === 'bordertype' ?
            localBorderCard[item.text].type = data.value :
            localBorderCard[item.text].size = data.value
        }
        this.setState({ borderCard: localBorderCard })
    }

    handleChangeColorTextInput(color) {
        this.setState({ colorTextInput: color.hex })
    }

    handleChangeColorBackgroundInput(color) {
        this.setState({ colorBackgroundInput: color.hex })
    }

    handleChangeColorBorder(color, item) {
        let localBorderCard = this.state.borderCard
        if (item.text === 'Tutti') {
            for (let index in localBorderCard) {
                localBorderCard[index].color = color.hex
            }
        } else {
            localBorderCard[item.text].color = color.hex
        }
        this.setState({ borderCard: localBorderCard })
    }

    handleChangeQueryParams(param) {
        let localQueryParams = Object.assign({}, this.state.queryParams)
        localQueryParams[param] = !localQueryParams[param]
        this.setState({queryParams: localQueryParams})
    }

    saveNewProfile() {
        let {modalVisible, errorMessage, share, indexForm, iconColor, ...formData} = this.state
        this.createProfile(formData)
        this.setState({modalVisible: !this.state.modalVisible})
    }

    // Crea un nuovo profilo
    createProfile(data) {
        let newData = data
        newData.newProfileName = data.newProfileName.replace(/"/g, '')
        newData.layout.layout_margins = isValidJSON(newData.layout.layout_margins) ? JSON.parse(newData.layout.layout_margins) : newData.layout.layout_margins
        let profile_name = data.newProfileName
        let profile_conf = escapeQuotes(newData)
        let query = `
        mutation createNewProfile {
            createCaaProfile(profile_name: "${profile_name}",
                            profile_system: 0,
                            profile_conf: "${profile_conf}",
                            profile_user_id: ${this.props.user.id},
                            profile_team_id: ${this.props.teamID}) {
                id
            }
        }
        `
        this.props.createNewProfile(query)
    }

    updateProfile() {
        let {modalVisible, errorMessage, share, indexForm, iconColor, ...formData} = this.state
        this.updateCurrentProfile(formData)
        this.setState({modalVisible: !this.state.modalVisible})
    }

    // Aggiorna un profilo preesistente
    updateCurrentProfile(data) {
        if (this.props.updateProjectProfile) {
            this.props.updateProjectProfile(data)
        } else {
            let newData = data
            newData.newProfileName = data.newProfileName.replace(/"/g, '')
            let profile_name = data.newProfileName
            let profile_conf = escapeQuotes(newData)
            let query = `
            mutation updateProfile {
                updateCaaProfile(id: ${this.props.id},
                    profile_name: "${profile_name}",
                    profile_conf: "${profile_conf}") {
                        id
                    }
                }
                `
            this.props.updateProfile(query)
        }
    }

    resetBorderAll() {
        let border = {
            Aggettivo: {color: '',size: '1',type: ''},
            Articolo: {color: '',size: '1',type: ''},
            Avverbio: {color: '',size: '1',type: ''},
            Congiunzione: {color: '',size: '1',type: ''},
            Interiezione: {color: '',size: '1',type: ''},
            Pronome: {color: '',size: '1',type: ''},
            Preposizione: {color: '',size: '1',type: ''},
            Sostantivo: {color: '',size: '1',type: ''},
            Verbo: {color: '',size: '1',type: ''},
            Altro: {color: '',size: '1',type: ''},
            Tutti: {color: '',size: '1',type: ''}
        }
        this.setState({borderCard: border})
    }

    render() {
        const { t } = this.props
        let FormContent = []
        let FormButton = []
        let iconModal

        if (this.props.edit && this.props.edit === 'icon') {
            iconModal = <Icon name='edit'
                style={this.props.style}
                className={this.props.className}
                size={this.props.size}
                onClick={this.handleOpenCloseModal.bind(this, 'open')}
                disabled={this.props.disabled}
            />
        } else if (this.props.edit && this.props.edit === 'button') {
            iconModal = <Button onClick={this.handleOpenCloseModal.bind(this, 'open')} disabled={this.props.disabled}>
                            {t("PRJ_MNU_OPTIONS")}
                        </Button>
        } else if (this.props.type && this.props.type === 'dropdown') {
            iconModal = <Dropdown.Item
                            style={this.props.style}
                            className='icon-pointer dropdown-item-hover'
                            onClick={this.handleOpenCloseModal.bind(this, 'open')}
                            disabled={this.props.disabled}
                        >
                            {t("PRJ_MNU_ADDPROFILE")}
                        </Dropdown.Item>
        } else {
            iconModal = <Icon name='add square'
                style={this.props.style}
                className={this.props.className}
                size={this.props.size}
                color={this.state.iconColor}
                onMouseOver={this.onMouseOverIcon.bind(this)}
                onMouseOut={this.onMouseOverIcon.bind(this)}
                onClick={this.handleOpenCloseModal.bind(this, 'open')}
                disabled={this.props.disabled}
            />
        }

        FormContent[0] = <div>
            <Form>
                <Form.Field required>
                    <label>{t("MAIN_FRM_PROFILENAME")}</label>
                    <input placeholder={t("MAIN_FRM_PROFILENAME")}
                        value={this.state.newProfileName}
                        onChange={this.handleFormChange.bind(this)}
                        maxLength={55}
                    />
                </Form.Field>
            </Form>
            <Message
                hidden={this.state.errorMessage}
                error
                header='Error'
                content={this.props.t("ERR_PROFILE_TITLE")}
            />
        </div>

        FormContent[1] = <ProfileOptions
            changeOtions={this.handleChangeOption}
            changeOrder={this.handleChangeOrder}
            changeQueryParams={this.handleChangeQueryParams}
            order={this.state.priorityOrder}
            imgType={this.state.imgType}
            SymStyle={this.props.symstyle}
            selectedSymStyle={this.state.imgStyle}
            queryParams={this.state.queryParams}
        />
        FormContent[2] = <MarginOptions
            optionsLayouts={this.props.optionsLayouts}
            layout={this.state.layout}
            updateLayout={this.handleChangeLayout}
        />

        FormContent[3] = <InputOptions
            changeOtions={this.handleChangeOption}
            changeColorBack={this.handleChangeColorBackgroundInput}
            changeColorText={this.handleChangeColorTextInput}
            sizeInput={this.state.sizeInput}
            posInput={this.state.posInput}
            transparent={this.state.styleInput}
            formatInput={this.state.formatInput}
            weightInput={this.state.weightInput}
            decorationInput={this.state.decorationInput}
            fontStyleInput={this.state.fontStyleInput}
            colorTextInput={this.state.colorTextInput}
            colorBackgroundInput={this.state.colorBackgroundInput}
            Style={this.state.cardStyle}
        />

        FormContent[4] = <ImageOptions
            changeOtions={this.handleChangeOption}
            sizeInput={this.state.sizeInput}
            posInput={this.state.posInput}
            transparent={this.state.styleInput}
            formatInput={this.state.formatInput}
            weightInput={this.state.weightInput}
            decorationInput={this.state.decorationInput}
            fontStyleInput={this.state.fontStyleInput}
            colorTextInput={this.state.colorTextInput}
            colorBackgroundInput={this.state.colorBackgroundInput}
            imgSize={this.state.imgSize}
            imgPadding={this.state.imgPadding}
            Style={this.state.cardStyle}
        />

        FormContent[5] = <CardOptions
            changeOtions={this.handleChangeBorder}
            changeColorBorder={this.handleChangeColorBorder}
            borderCard={this.state.borderCard}
            sizeInput={this.state.sizeInput}
            resetAll={this.resetBorderAll}
            posInput={this.state.posInput}
            transparent={this.state.styleInput}
            formatInput={this.state.formatInput}
            weightInput={this.state.weightInput}
            decorationInput={this.state.decorationInput}
            fontStyleInput={this.state.fontStyleInput}
            colorTextInput={this.state.colorTextInput}
            colorBackgroundInput={this.state.colorBackgroundInput}
            Style={this.state.cardStyle}
        />

        let panes = FormContent.map((item, index) => {
            return { menuItem: this.props.t('OPT_LBL_TAB' + index), render: () => <Tab.Pane>{item}</Tab.Pane> }
        })

        return (
            <div>
                <Modal trigger={iconModal} closeOnDimmerClick={false} open={this.state.modalVisible} size='large'>
                    <Modal.Header>{this.props.edit ? t("MAIN_LBL_UPDATEPROFILE") : t("MAIN_LBL_CREATEPROFILE")}</Modal.Header>
                    <Modal.Content>
                        <Tab panes={panes} activeIndex={this.state.indexForm} onTabChange={this.handleTabChange.bind(this)}/>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button.Group>
                            <Button color='green' onClick={this.props.edit ? this.updateProfile.bind(this) : this.saveNewProfile.bind(this)}>
                                {this.props.edit ? t("MAIN_BTN_UPDATEPROFILE") : t("MAIN_BTN_CREATEPROFILE")}
                            </Button>
                            <Button.Or />
                            <Button color='red' onClick={this.handleOpenCloseModal.bind(this, 'close')}>
                                {t("MAIN_BTN_CLOSE")}
                            </Button>
                        </Button.Group>
                    </Modal.Actions>
                </Modal>
            </div>
        )
    }
}

NewProfileForm.propTypes = {
    updateProjectProfile: PropTypes.func,
    data: PropTypes.string,
    className: PropTypes.string,
    size: PropTypes.string,
    edit: PropTypes.string,
    disable: PropTypes.bool,
    type: PropTypes.string,
    id: PropTypes.number,

}

const mapStateToProps = (state) => {
    return {
        symstyle: state.symstyleData,
        optionsLayouts: state.layoutData,
        user: state.user,
        teamID: state.filterValue,
        teams: state.userTeamData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createNewProfile: (query) => dispatch(profileDataCreateData(query)),
        updateProfile: (query) => dispatch(profileDataUpdateData(query)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(NewProfileForm))
