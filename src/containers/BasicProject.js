import React, { Component } from 'react'
import { Message, Menu, Loader, Dimmer, Transition, Icon } from 'semantic-ui-react'

import NavBarEdit from './edit/NavBarEdit'
import BasicProjectNavBar from './edit/BasicProjectNavBar'
import CardUI from './edit/CardUI'
import Speak from '../components/speak/Speak'

import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'

import { connect } from 'react-redux'
import { chapterDataUpdateData, chapterDataFetchData } from '../store/actions/ChapterFetchActions'
import { CardUINavbarCard, selectedProjectID, CardUIsaveIsCompleted } from '../store/actions/CardUIActions'
import { setNavBar } from '../store/functionUtils'
import { dispatchTypoImgSize } from '../store/actions/TypoActions'

class BasicProject extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mode: this.props.match.params.mode === 'edit' ? true : false,
            rowStyle: {},
            // Unsaved Card Variables
            cardStyle: {},
            posInput: 'bottom',
            sizeInput: 'small',
            styleInput: 'normal',
            formatInput: 'freeInput',
            colorTextInput: '#000000',
            colorBackgroundInput: '#FFFFFF',
            weightInput: 'weightNormalInput',
            decorationInput: 'decorationNormalInput',
            fontStyleInput: 'styleNormalInput',
            imgSize: 'small',
            imgPadding: 'imgpadding1',
            imgType: 0,
            imgStyle: 0,
            priorityOrder: [{text: 'type'},{text: 'color'}],
            borderCard: {
                Aggettivo: {color: '',size: '1',type: ''},
                Articolo: {color: '',size: '1',type: ''},
                Avverbio: {color: '',size: '1',type: ''},
                Congiunzione: {color: '',size: '1',type: ''},
                Interiezione: {color: '',size: '1',type: ''},
                Pronome: {color: '',size: '1',type: ''},
                Preposizione: {color: '',size: '1',type: ''},
                Sostantivo: {color: '',size: '1',type: ''},
                Verbo: {color: '',size: '1',type: ''},
                Altro: {color: '',size: '1',type: ''}
            },
            layout: {},
            urlRest: window.env.GraphQLServer,
            urlImg: window.env.PathImages,
            firstMount: true,
        }
        this.handleMode = this.handleMode.bind(this)
    }

    componentDidMount() {
        // Setto i dati del profilo
        let index = this.props.project.findIndex(x => x.proj_id === parseInt(this.props.match.params.projectid, 10))
        if (this.props.project[index].proj_profile) {
            let data = JSON.parse(this.props.project[index].proj_profile)
            this.setState({...data})
            this.props.setTypoDropdown(data.imgSize)
        }

        let query
        if (this.state.mode === true) {
            // Block the chapter
            query = `
            mutation BlockChapter {
                updateCaaChapter(id: ${this.props.match.params.chapterid}, chapt_user_block: ${this.props.user.id}) {
                    id
                }
            }
            `
            this.props.blockChapter(query)
        }

        // Fetch chapter content
        query = `
            query CurrentChapter {
                chapters (id: ${this.props.match.params.chapterid}) {
                    data {
                        id
                        caa_project_id
                        chapt_title
                        chapt_content
                        chapt_row
                        chapt_typo
                        chapt_user_block
                    }
                }
            }
        `
        this.props.fetchChapterContent(query, null, null, null, true)

        var navbarInterval = setInterval(() => {
            if (!this.props.chapterHasErrored && !this.props.chapterIsLoading) {
                // Setta la distanza della card dai menù
                let navMenu = document.getElementById('navbar')
                let navbarHeight = navMenu.offsetHeight + 10
                let style = { 'marginTop': navbarHeight + 'px'}
                this.setState({rowStyle: style, firstMount: false})
                // Setta la navbar card di default
                this.props.setNavbarCard(0)
                clearInterval(navbarInterval)
            }
        }, 100);

        this.props.setCurrentProjectID(this.props.match.params.projectid)
    }

    // HANDLE EDIT-VIEW MODE
    handleMode() {
        this.setState({ mode: !this.state.mode}, () => {
            setNavBar(this.state.mode)
        })
    }

    render() {
        let index = this.props.project.findIndex(x => x.proj_id === parseInt(this.props.match.params.projectid, 10))
        let indexChapter = this.props.chapter.findIndex(x => x.id === parseInt(this.props.match.params.chapterid, 10))
        let chapterName = this.props.chapter[indexChapter].chapt_title

        // Call save message
        let saveMessageHidden = true
        if (this.props.saveMessage === true) {
            saveMessageHidden = false
            let self = this
            setTimeout(() => {
                saveMessageHidden = true
                self.props.saveMessageComplete(false)
            }, 1000)
        }

        let menu = <div className="no-print">
            <Menu id='navbar' style={{'marginBottom': '10px', 'width': '100%'}}
                vertical
                fixed='top'
                >
                <Menu.Item className='navbar-button'>
                    <NavBarEdit
                        checkMode={this.handleMode}
                        checked={this.state.mode}
                        projName={this.props.project[index].proj_name}
                        chaptName={chapterName}
                    />
                </Menu.Item>
                <BasicProjectNavBar mode={this.state.mode} />
                <Menu.Item className='navbar-speak fix-display viewMode' id='navbar-speak'>
                    <Speak />
                </Menu.Item>
            </Menu>
        </div>

        if ((this.props.chapterHasErrored || this.props.chapterIsLoading) && this.state.firstMount) {
            return (
                <div>
                    {menu}
                    <Dimmer active={this.props.chapterIsLoading} page>
                        <Loader active inline='centered' size='massive' />
                    </Dimmer>
                </div>
            )
        } else {
            return (
                <div>
                    {menu}
                    <Transition animation='fly right' duration={1500} visible={!saveMessageHidden}>
                        <Message icon
                            className='saveMessage'
                            positive
                            compact={true}
                            hidden={saveMessageHidden}
                        >
                          <Icon name='save' style={{margin: '0px'}} />
                          <Message.Content>
                            <Message.Header>{this.props.t("EDIT_CHAPT_SAVED")}</Message.Header>
                          </Message.Content>
                        </Message>
                    </Transition>
                    <CardUI
                        rowStyle={this.state.rowStyle}
                        saveComplete={this.savedSuccessfully}
                        mode={!this.state.mode}
                        transparent={this.state.styleInput}
                        sizeInput={this.state.sizeInput}
                        posInput={this.state.posInput}
                        formatInput={this.state.formatInput}
                        weightInput={this.state.weightInput}
                        decorationInput={this.state.decorationInput}
                        fontStyleInput={this.state.fontStyleInput}
                        colorTextInput={this.state.colorTextInput}
                        colorBackgroundInput={this.state.colorBackgroundInput}
                        imgSize={this.state.imgSize}
                        imgPadding={this.state.imgPadding}
                        imgType={this.state.imgType}
                        imgStyle={this.state.imgStyle}
                        borderCard={this.state.borderCard}
                        urlRest={this.state.urlRest}
                        urlImg={this.state.urlImg}
                        priorityOrder={this.state.priorityOrder}
                        margins={this.state.layout.layout_margins}
                    />
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        project: state.projectData,
        chapter: state.chapterData,
        navbarCard: state.CardUINavbarCard,
        chapterIsLoading: state.chapterDataIsLoading,
        chapterHasErrored: state.chapterDataHasErrored,
        saveMessage: state.CardUIsaveIsCompleted,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        blockChapter: (mutation) => dispatch(chapterDataUpdateData(mutation)),
        fetchChapterContent: (query, limit, page, projectId, content) => dispatch(chapterDataFetchData(query, limit, page, projectId, content)),
        setNavbarCard: (value) => dispatch(CardUINavbarCard(value)),
        setCurrentProjectID: (id) => dispatch(selectedProjectID(id)),
        saveMessageComplete: (value) => dispatch(CardUIsaveIsCompleted(value)),
        setTypoDropdown: (size) => dispatch(dispatchTypoImgSize(size)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(withRouter(BasicProject)))
