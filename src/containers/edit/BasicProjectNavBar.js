import React, { Component, Fragment } from 'react'
import { Icon, Popup, Menu, Segment, Dropdown, Button } from 'semantic-ui-react'
import { translate } from 'react-i18next'

import UploadSymbol from '../UploadSymbol'
import Can from '../Permission'

import { connect } from 'react-redux'
import { AddNewBlankCard, toggleVisibilityImgAlt, mergeCard, unlinkCard, blockCard, copyCard, pasteCard, searchLemma, jumpRow, deleteCard, addNewRow, editRow, handleWatermark } from '../../store/actions/CardUIActions'
import { setNavBar } from '../../store/functionUtils'

class BasicProjectNavBar extends Component {
    componentDidMount() {
        setNavBar(this.props.mode)
    }

    render() {
        const { t } = this.props
        let navbarCard = Object.assign({}, this.props.navbarCard)

        let srcImg = ''
        if (Object.keys(navbarCard).length !== 0 && navbarCard.constructor === Object) {
            let testCustom = navbarCard.imgAlt.find(x => x.img === navbarCard.img)
            srcImg = testCustom && testCustom.custom ?
                window.env.CustomImage + navbarCard.img :
                window.env.PathImages + navbarCard.img
        }

        return (
            <Menu.Item className='navbar-icon fix-all-display' id='navbar-icon'>
                <Can perform='can_manage_preload' or='can_upload_symbol'>
                    <Fragment>
                        <UploadSymbol type='icon'
                            mode='upload'
                            buttonActionTranslateText='UPSY_UPLOAD'
                        />
                        <UploadSymbol type='icon' card={navbarCard}
                            mode='save'
                            buttonActionTranslateText='UPSY_SAVE'
                        />
                    </Fragment>
                </Can>
                <Popup
                    trigger={<Icon name='arrow left' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.mergeCard('left')}}
                        disabled={navbarCard.water ? true : false}
                        />}
                    content={t("POPUP_MERGE_SX")}
                />

                <Popup
                    trigger={<Icon name='arrow right' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.mergeCard('right')}}
                        disabled={navbarCard.water ? true : false}
                        />}
                    content={t("POPUP_MERGE_DX")}
                />

                <Popup
                    trigger={<Icon name='unlinkify' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.unlinkCard()}}
                        disabled={navbarCard.water ? true : false}
                        />}
                    content={t("POPUP_UNLINK")}
                />

                <Popup
                    trigger={<Dropdown
                            icon={{name: 'bookmark', size:'large', color:'teal', bordered: true, inverted:true}}
                            style={{marginLeft: '.5em'}}
                            disabled={navbarCard.water ? true : false}
                        >
                            <Dropdown.Menu>
                                <Dropdown.Item  text='Reset' onClick={() => this.props.handleWatermark(null, true)} />
                                <Dropdown.Item  text='Negazione' onClick={() => this.props.handleWatermark('negazione')} />
                                <Dropdown.Item  text='Plurale' onClick={() => this.props.handleWatermark('plurale')} />
                                <Dropdown.Item  text='Passato' onClick={() => this.props.handleWatermark('passato')} />
                                <Dropdown.Item  text='Futuro' onClick={() => this.props.handleWatermark('futuro')} />
                            </Dropdown.Menu>
                        </Dropdown>}
                    content={t("POPUP_WATERMARK")}
                />
                
                <Popup
                    trigger={<Icon name='image' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.toggleVisibilityImgAlt()}}
                        disabled={Object.keys(navbarCard).length !== 0 && navbarCard.imgAlt.length <= 1 ? true : false}
                        />}
                    content={t("POPUP_SHOW")}
                />

                <Popup
                    trigger={<Icon name={navbarCard.lock} bordered inverted
                        color={navbarCard.lock === 'lock' ? 'red' : 'teal'}
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.blockCard()}}
                        disabled={navbarCard.water ? true : false}
                        />}
                    content={t("POPUP_LOCK")}
                />

                <Popup
                    trigger={
                        <Icon name='copy' bordered inverted color="teal"
                            size='large'
                            className="icon-pointer"
                            onClick={() => {this.props.copyCard()}}
                            disabled={navbarCard.water ? true : false}
                            />}

                    content={t("POPUP_COPY")}
                />

                <Popup
                    trigger={
                        <Icon name='paste' bordered inverted color="teal"
                            size='large'
                            className="icon-pointer"
                            onClick={() => {this.props.pasteCard()}}
                            disabled={Object.keys(this.props.cardToPaste).length > 0 ? false : true}
                            />}

                    content={t("POPUP_PASTE")}
                />

                <Popup
                    trigger={
                        <Icon name='search' bordered inverted color='teal'
                            size='large'
                            onClick={() => {this.props.searchLemma()}}
                            className="icon-pointer"
                            disabled={navbarCard.lock === 'lock' || navbarCard.water ? true : false}
                            />
                    }
                    content={t("POPUP_SEARCH")}
                />

                <Popup
                    trigger={<Icon name='external share' flipped='horizontally' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.AddNewBlankCard(navbarCard.id, navbarCard.row)}}
                        />}
                    content={t("POPUP_ADD_SX")}
                />

                <Popup
                    trigger={<Icon name='external share' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.AddNewBlankCard(navbarCard.id + 1, navbarCard.row)}}
                        />}
                    content={t("POPUP_ADD_DX")}
                />

                <Popup
                    trigger={<Icon name='external share' bordered inverted color="teal"
                        style={{'transform': 'rotate(180deg)'}}
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.jumpRow()}}
                        disabled={navbarCard.lemma === '' ? true : false}
                        />}
                    content={t("POPUP_JUMP_ROW")}
                />

                <Popup
                    trigger={<Icon name='sticky note' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.editRow(navbarCard.row, 'page')}}
                        />}
                    content={t("POPUP_NEW_PAGE")}
                />

                <Popup
                    trigger={<Icon name='delete' bordered inverted color="teal"
                        size='large'
                        className="icon-pointer"
                        onClick={() => {this.props.deleteCard()}}
                        />}
                    content={t("POPUP_DELETE")}
                />

                <img src={navbarCard.water ? window.env.WaterImages + navbarCard.img : srcImg}
                    style={{'verticalAlign': 'middle',
                            'marginRight': '10px',
                            'marginLeft': '10px'}}
                    width='35'
                    alt='symbol preview'
                />
                {navbarCard.lemma}

                <Segment basic className='float-right no-padding nav-icon-margin'>
                    <Popup
                        trigger={<Icon name='minus circle' bordered inverted color="teal"
                            size='large'
                            className="icon-pointer"
                            onClick={() => {this.props.editRow(navbarCard.row, 'subtract')}}
                            />}
                        content={t("POPUP_SUBTRACT_MARGIN")}
                    />
                </Segment>
                <Segment basic className='float-right no-padding no-margin'>
                    <Popup
                        trigger={<Icon name='plus circle' bordered inverted color="teal"
                            size='large'
                            className="icon-pointer"
                            onClick={() => {this.props.editRow(navbarCard.row, 'add')}}
                            />}
                        content={t("POPUP_ADD_MARGIN")}
                    />
                </Segment>

                {
                // <Segment basic className='float-right no-padding nav-icon-margin'>
                //     <Popup
                //         trigger={<Icon name='external share' flipped='horizontally' bordered inverted color="teal"
                //             size='large'
                //             className="icon-pointer"
                //             onClick={() => {this.props.addBeforeAfterCard('before')}}
                //             />}
                //         content={t("POPUP_ADD_SX")}
                //     />
                // </Segment>
                //
                // <Segment basic className='float-right no-padding nav-icon-margin'>
                //     <Popup
                //         trigger={<Icon name='external share' bordered inverted color="teal"
                //             size='large'
                //             className="icon-pointer"
                //             onClick={() => {this.props.addBeforeAfterCard('after')}}
                //             />}
                //         content={t("POPUP_ADD_DX")}
                //     />
                // </Segment>
                // <Segment basic className='float-right no-padding nav-icon-margin'>
                //     <Popup
                //         trigger={<Icon name='plus circle' bordered inverted color="teal"
                //             size='large'
                //             className="icon-pointer"
                //             // onClick={() => {this.props.addBeforeAfterCard('after')}}
                //             />}
                //         content={t("POPUP_ADD_DX")}
                //     />
                // </Segment>
                // <Segment basic className='float-right no-padding no-margin'>
                //     <Popup
                //         trigger={<Icon name='minus circle' bordered inverted color="teal"
                //             size='large'
                //             className="icon-pointer"
                //             // onClick={() => {this.props.addBeforeAfterCard('after')}}
                //             />}
                //         content={t("POPUP_ADD_DX")}
                //     />
                // </Segment>
                }
            </Menu.Item>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        project: state.projectData,
        navbarCard: state.CardUINavbarCard,
        cardToPaste: state.CardUIcopyPaste,
        chapterIsLoading: state.chapterDataIsLoading,
        chapterHasErrored: state.chapterDataHasErrored,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleVisibilityImgAlt: (value) => dispatch(toggleVisibilityImgAlt(value)),
        mergeCard: (direction, id , lemma) => dispatch(mergeCard(direction, id , lemma)),
        unlinkCard: () => dispatch(unlinkCard()),
        blockCard: () => dispatch(blockCard()),
        copyCard: () => dispatch(copyCard()),
        pasteCard: () => dispatch(pasteCard()),
        searchLemma: () => dispatch(searchLemma()),
        deleteCard: () => dispatch(deleteCard()),
        jumpRow: () => dispatch(jumpRow()),
        addNewRow: (row, type) => dispatch(addNewRow(row, type)),
        editRow: (row, actions) => dispatch(editRow(row, actions)),
        AddNewBlankCard: (index, row) => dispatch(AddNewBlankCard(index, row)),
        handleWatermark: (watermark, reset) => dispatch(handleWatermark(watermark, reset)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(translate('translations')(BasicProjectNavBar))
