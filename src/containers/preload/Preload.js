import React, { Component } from 'react'
import { Menu, Button, Form, Statistic, Container, Pagination, Dropdown } from 'semantic-ui-react'
import { translate } from 'react-i18next'
import { withRouter } from 'react-router-dom'

import { connect } from 'react-redux'

import { apolloFetch } from '../../store/apolloFetchUtils'
import PreloadTable from '../../components/preload/PreloadTable'

class Preload extends Component {
    constructor(props) {
        super(props)
        this.state = {
            valueSearch: '',
            preloadData: [],
            total: 0,
            totalPages: 1,
            page: 1,
            filter: -1,
            filterOptions: [{value: -1, text: props.t("PRLD_DROP_ALL")}, {value: 0, text: props.t("PRLD_DROP_PRIVATE")}, {value: 1, text: props.t("PRLD_DROP_PRIVACY")}],
        }
    }

    componentDidMount() {
        // Set the options for the filter
        let localFilter = this.state.filterOptions
        if (this.props.user.user_permissions.can_manage_preload) {
            localFilter.push({value: 2, text: this.props.t("PRLD_DROP_TEAM")})
            if (this.props.user.user_permissions.can_manage_staff) {
                localFilter.push({value: 3, text: this.props.t("PRLD_DROP_STAFF")})
            }
        }

        this.setState({filterOptions: localFilter})
    }

    // Handle change filter value
    dropdownChange(e, data) {
        this.setState({filter: data.value, page: 1}, () => {
            if (this.state.valueSearch === '') {
                this.displayFullPreload(true)
            } else {
                this.search(null, true)
            }
        })
    }

    // Handle change of search input bar
    handleValue(event) {
        this.setState({valueSearch: event.target.value.toLowerCase()})
    }

    // Handle enter key in search bar
    handleEnter(event) {
        if (event.keyCode === 13) {
            event.preventDefault()
            this.search(null, false)
        }
    }

    // Handle refresh of the page after the delete or the edit of the record
    refreshAfterEditDelete() {
        if (this.state.valueSearch === '') {
            this.displayFullPreload(true)
        } else {
            this.search(null, true)
        }
    }

    // Main search preload
    search(customQuery = null, fromPagination = false) {
        let query, queryField
        if (customQuery && typeof customQuery === 'string') {
            query = customQuery
        } else {
            let extraQuery = ''
            if (this.state.filter !== -1) {
                extraQuery = ', type_img:' + this.state.filter
            }
            if (!this.props.user.user_permissions.can_manage_preload) {
                extraQuery += ', created_by: ' + this.props.user.id
            }

            if (this.state.valueSearch.slice(-4) === '.png') {
                queryField = 'symbol_sign: "' + this.state.valueSearch.trim() + '"'
            } else {
                queryField = 'voice_master: "' + this.state.valueSearch.trim() + '"'
            }
            query =`
            query fetchPreload {
                preload_manage(${queryField}, page: ${fromPagination === true ? this.state.page : 1}${extraQuery}) {
                    total
                    data {
                        id
                        voice_human
                        with_image
                        lexical_expr
                        idclass
                        symbol_sign
                        imgcolor
                        idstyle
                        created_by
                        username
                        staff_name
                        team_name
                        origin
                    }
                }
            }
            `
        }
        apolloFetch({ query })
            .then((data) => {
                this.setState({
                  preloadData: data.data.preload_manage.data,
                  total: data.data.preload_manage.total,
                  totalPages: Math.ceil(data.data.preload_manage.total / 15),
                  page: fromPagination === true ? this.state.page : 1
                });
            })
            .catch((error) => {
                console.log(error);
            })
    }

    // display full preload record
    displayFullPreload(fromPagination = false) {
        this.setState({valueSearch: ''}, () => {
            let extraQuery = ''
            if (this.state.filter !== -1) {
                extraQuery = ', type_img:' + this.state.filter
            }
            if (!this.props.user.user_permissions.can_manage_preload) {
                extraQuery += ', created_by: ' + this.props.user.id
            }

            let query =`
            query fetchFromView {
                preload_manage(page: ${fromPagination === true ? this.state.page : 1}${extraQuery}) {
                    total
                    data {
                        id
                        voice_human
                        with_image
                        lexical_expr
                        idclass
                        symbol_sign
                        imgcolor
                        idstyle
                        created_by
                        username
                        staff_name
                        team_name
                        origin
                    }
                }
            }
            `
            this.search(query, fromPagination)
        })
    }

    // Handle change page in pagination
    changePagePrelaod(e, activePage) {
        this.setState({page: activePage.activePage}, () => {
            if (this.state.valueSearch === '') {
                this.displayFullPreload(true)
            } else {
                this.search(null, true)
            }
        })
    }

    // delete current preload record
    deletePreloadRecord(item, e) {
        let query =`
        mutation deleteRecord {
            deleteCaaPreloadHeadword(id: ${item.id}) {
                id
            }
        }
        `
        this.state.apolloFetch({ query })
            .then((data) => {
                if (this.state.valueSearch === '') {
                    this.displayFullPreload()
                } else {
                    this.search()
                }
            })
            .catch((error) => {
                console.log(error);
            })
    }

    // redirect to homepage and clean the chapter list data
    redirectProject() {
        this.props.history.push('/home')
    }

    // MAIN RENDER
    render() {
        const { t } = this.props

        return (
            <div>
                <Menu>
                    <Menu.Item>
                        <Button color='red' onClick={this.redirectProject.bind(this)}>{t("PRJ_MNU_BACK")}</Button>
                    </Menu.Item>
                </Menu>
                <Container>
                    <Form>
                        <Form.Input
                            size='large'
                            action
                            placeholder={t("PRLD_LBL_INPUT_WORD")}
                            value={this.state.valueSearch}
                            onChange={this.handleValue.bind(this)}
                            onKeyDown={this.handleEnter.bind(this)}
                        >
                            <input />
                            {
                                this.props.user.user_permissions.can_manage_preload ?
                                <Dropdown placeholder='Filter' selection
                                    options={this.state.filterOptions}
                                    value={this.state.filter}
                                    onChange={this.dropdownChange.bind(this)}
                                />
                                : null
                            }
                            <Button color="blue" onClick={this.search.bind(this)}>{t("PRLD_LBL_BUTTON_SEARCH")}</Button>
                            <Button color="teal" onClick={this.displayFullPreload.bind(this)}>
                                {t("PRLD_LBL_BUTTON_ALL")}
                            </Button>
                        </Form.Input>
                    </Form>
                </Container>
                <Statistic.Group widths={2}>
                    <Statistic color='blue' floated='right'>
                        <Statistic.Value>{this.state.preloadData.length}</Statistic.Value>
                        <Statistic.Label>{t("PRLD_LBL_STATISTIC_LIMIT")}</Statistic.Label>
                    </Statistic>
                    <Statistic color='blue' floated='right'>
                        <Statistic.Value>{this.state.total}</Statistic.Value>
                        <Statistic.Label>{t("PRLD_LBL_STATISTIC_TOTAL")}</Statistic.Label>
                    </Statistic>
                </Statistic.Group>
                <Container textAlign='center'>
                    <Pagination
                        activePage={this.state.page}
                        totalPages={this.state.totalPages}
                        onPageChange={this.changePagePrelaod.bind(this)}
                        style={{marginTop: '20px'}}
                    />
                </Container>
                <PreloadTable data={this.state.preloadData} refresh={this.refreshAfterEditDelete.bind(this)} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(translate('translations')(Preload)))
