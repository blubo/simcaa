import { createApolloFetch } from 'apollo-fetch'
import { jwtExpiredOrInvalid } from '../store/actions/JWTActions'

import store from './configureStore'

let uri = window.env.GraphQLServer
const apolloFetch = createApolloFetch({uri})

uri = window.env.GraphQLServerNoAuth
const apolloFetchNoAuth = createApolloFetch({uri})

apolloFetch.use(({ request, options }, next) => {
    const token = sessionStorage.getItem('jwt')
    if (!options.headers) {
        options.headers = {}; // Create the headers object if needed.
    }
    options.headers['authorization'] = `Bearer ${token}`
    next()
})

apolloFetch.useAfter(({ response }, next) => {
    if (response.headers.get('Authorization') !== null) {
        sessionStorage.setItem('jwt', response.headers.get('Authorization'))
    } else if (response.parsed.error === 'token_expired' || response.parsed.error === 'token_invalid') {
        sessionStorage.removeItem('jwt')
        store.dispatch(jwtExpiredOrInvalid(true))
    }
    next()
})

export {apolloFetch, apolloFetchNoAuth}
