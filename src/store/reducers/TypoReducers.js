import { TYPO_SELECTED_IMGSIZE } from '../constants'

export function selectedImgSize(state = 'small', action) {
    switch (action.type) {
        case TYPO_SELECTED_IMGSIZE:
            return action.size

        default:
            return state
    }
}
