import { FILTER_PROJECT_MODE, FILTER_PROJECT_VALUE, FILTER_PROJECT_PAGINATION_PAGE } from '../constants'

export function filterMode(state = 'private', action) {
    switch (action.type) {
        case FILTER_PROJECT_MODE:
            return action.mode

        default:
            return state
    }
}

export function filterValue(state = 1, action) {
    switch (action.type) {
        case FILTER_PROJECT_VALUE:
            return action.value

        default:
            return state
    }
}

export function projectPaginationPage(state = 1, action) {
    switch (action.type) {
        case FILTER_PROJECT_PAGINATION_PAGE:
            return action.value
        default:
            return state
    }
}
