let dominio = "http://10.0.0.132"
let ambiente = ""
let graphql_url = "http://10.0.0.132:8085"

// let dominio = "https://www.simcaa.it"
// let ambiente = "demo" + "/"
// let graphql_url = dominio + "/" + ambiente + "apisimcaa-graphql/public"

window.env = {
    // MEDIA
    "PathImages": dominio + "/media/symbols/",
    "WaterImages": dominio + "/media/specialchar/",
    "CustomImage": dominio + "/" + ambiente + "/media/custom/",
    "ApiImageUpload": graphql_url + "/graphql/imageupload",
    "ApiSymbolUpload": graphql_url + "/graphql/imageuploadtmp",
    "MediaImage": dominio + "/" + ambiente + "media/images/",
    "TmpImage": dominio + "/" + ambiente + "media/tmp/",

    // AWS S3
    // "PathImages": "https://simcaa-repo-media.s3.eu-central-1.amazonaws.com/smc-arasaac-official/",
    // "CustomImage": "https://simcaa-repo-media.s3.eu-central-1.amazonaws.com/smc-custom/",
    
    // API
    "GraphQLServer": graphql_url + "/graphql/query",
    "GraphQLServerNoAuth": graphql_url + "/graphql/query/noauth",
    "GraphQLLogin": graphql_url + "/graphql/login",
    "GraphQLMail": graphql_url + "/graphql/mail",
    "GraphQLCurrentUser": graphql_url + "/graphql/me",
    "GraphQLRefreshToken": graphql_url + "/graphql/jwtrefresh",
    "RestApiCard": graphql_url + "/graphql/card",
    "RestApiNotInTeam": graphql_url + "/graphql/notInTeam",
    "RestApiDuplicateProject": graphql_url + "/graphql/duplicateproject",
    "RestApiDuplicateChapter": graphql_url + "/graphql/duplicatechapter",

    // CSS
    "SemanticCSS": "./semantic.min.css",
    "StyleCSS": "./style.css",

    // Responsive Voice URL
    "responsiveVoiceUrl": "http://code.responsivevoice.org/responsivevoice.js",
    // "responsiveVoiceUrl": "",

    // Global Variables
    "demoMode": false,
    "maintenanceMode": false,

    // MAINTENANCE MESSAGE
    "maintenanceMessage": `
    <h1>
    <b>Manutenzione !!!</b>
    <p>manutenzione SimCAA</p>
    </h1>
    `,

    // HOMEPAGE MESSAGE
    "blue_text_header": "To test this Demo",
    "blue_text_content": "<p>Username: user</p>Password: user",
    "violet_text_header": "OpenLab Asti",
    "violet_text_content": `Stiamo sviluppando l&#39;applicazione SIMCAA
        "Scrittura Inclusiva Multimodale Comunicazione Aumentativa Aperta"
        Questo sito è un ambiente di demo/test di <a href='http://openlabasti.it'> OpenLab Asti </a>.
        SIMCAA è in corso di sviluppo, quindi soggetta ad aggiornamenti e
        modifiche continue, le funzionalità sono incomplete e non sono garantite,
        è possibile l&#39;interruzione temporanea del servizio.
        Non si assicura la persistenza dei documenti caricati.
        I simboli usati sono di proprietà del Governo di Aragona,
        creati da Sergio Palao per <a href='http://arasaac.org'> ARASAAC</a> e distribuiti con Licenza
        <a href='https://it.wikipedia.org/wiki/Creative_Commons'> Creative Common</a> (BY-NC-SA)`,
}
